200.2.1 = {
	holder = 1000018
	law = succ_feudal_elective
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000019
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1000647
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.4 = {
	liege = d_tanampa
}

200.2.5 = {
	holder = 1000646
	law = succ_primogeniture
	law = agnatic_succession
}

207.2.1 = {
	holder = 1001563
}

210.2.1 = {
	holder = 1000585
}

211.2.1 = {
	holder = 1002554
}

247.2.1 = {
	holder = 1002883
}

281.2.1 = {
	holder = 1005839
}

322.2.1 = {
	holder = 1005912
}

336.2.1 = {
	holder = 1007884
}

360.2.1 = {
	holder = 1008845
}

368.2.1 = {
	holder = 1009919
}

421.2.1 = {
	holder = 1010711
}

440.2.1 = {
	holder = 1012683
}

440.2.2 = {
	holder = 1013553
}

486.2.1 = {
	holder = 1015781
}

513.2.1 = {
	holder = 1015831
}

528.2.1 = {
	holder = 1016574
}

528.2.2 = {
	holder = 1017822
}

535.2.1 = {
	holder = 1017320
}

535.2.2 = {
	holder = 1018214
}

591.2.1 = {
	holder = 1018619
}

617.2.1 = {
	holder = 1020938
}

637.2.1 = {
	holder = 1023103
}

652.2.1 = {
	holder = 1023352
}

678.2.1 = {
	holder = 1024118
}

733.2.1 = {
	holder = 1025015
}

743.2.1 = {
	holder = 1027759
}

749.2.1 = {
	holder = 1027323
}

750.2.1 = {
	holder = 1028449
}

778.2.1 = {
	holder = 1028767
}

789.2.1 = {
	holder = 1029967
}

826.2.1 = {
	holder = 1030789
}

859.2.1 = {
	holder = 1032355
}

882.2.1 = {
	holder = 1034201
}

896.2.1 = {
	holder = 1034136
}

896.2.2 = {
	holder = 1035473
}

935.2.1 = {
	holder = 1036046
}

965.2.1 = {
	holder = 1038740
}

998.2.1 = {
	holder = 1039034
}

1031.2.1 = {
	holder = 1040495
}

1060.2.1 = {
	holder = 1042162
}

