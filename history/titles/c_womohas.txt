200.2.1 = {
	holder = 1000003
	law = succ_gavelkind
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000004
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1000223
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.4 = {
	liege = d_teru
}

200.2.5 = {
	holder = 1000222
	law = succ_elective_gavelkind
	law = agnatic_succession
}

247.2.1 = {
	holder = 1001460
}

264.2.1 = {
	holder = 1003418
}

265.2.1 = {
	holder = 1005110
}

265.2.2 = {
	holder = 1005002
}

288.2.1 = {
	holder = 1005018
}

293.2.1 = {
	holder = 1003690
}

297.2.1 = {
	holder = 1005518
}

298.2.1 = {
	holder = 1006654
}

301.2.1 = {
	holder = 1006207
}

305.2.1 = {
	holder = 1006980
}

364.2.1 = {
	holder = 1006987
}

369.2.1 = {
	holder = 1009859
}

409.2.1 = {
	holder = 1011849
}

430.2.1 = {
	holder = 1013077
}

468.2.1 = {
	holder = 1014883
}

485.2.1 = {
	holder = 1014895
}

508.2.1 = {
	holder = 1016134
}

537.2.1 = {
	holder = 1017168
}

575.2.1 = {
	holder = 1019050
}

579.2.1 = {
	holder = 1019002
}

580.2.1 = {
	holder = 1020314
}

620.2.1 = {
	holder = 1020923
}

643.2.1 = {
	holder = 1021667
}

643.2.2 = {
	holder = 1023339
}

679.2.1 = {
	holder = 1023909
}

700.2.1 = {
	holder = 1025354
}

709.2.1 = {
	holder = 1026267
}

711.2.1 = {
	holder = 1025851
}

712.2.1 = {
	holder = 1023906
}

800.2.1 = {
	holder = 1028253
}

800.2.2 = {
	holder = 1030821
}

816.2.1 = {
	holder = 1031210
}

816.2.2 = {
	holder = 1031752
}

1000.2.1 = {
	holder = 1040181
}

1017.2.1 = {
	holder = 1038546
}

1019.2.1 = {
	holder = 1041330
}

1064.2.1 = {
	holder = 1042540
}

