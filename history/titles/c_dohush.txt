200.2.1 = {
	holder = 1000039
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1001261
	law = succ_feudal_elective
	law = cognatic_succession
}

200.2.3 = {
	liege = d_ludada
}

200.2.4 = {
	holder = 1001260
	law = succ_feudal_elective
	law = cognatic_succession
}

240.2.1 = {
	holder = 1002798
}

268.2.1 = {
	holder = 1004622
}

279.2.1 = {
	holder = 1004024
}

280.2.1 = {
	holder = 1005816
}

309.2.1 = {
	holder = 1006191
}

358.2.1 = {
	holder = 1007754
}

375.2.1 = {
	holder = 1009774
}

391.2.1 = {
	holder = 1010880
}

457.2.1 = {
	holder = 1012239
}

463.2.1 = {
	holder = 1014460
}

529.2.1 = {
	holder = 1015704
}

543.2.1 = {
	holder = 1017928
}

572.2.1 = {
	holder = 1018785
}

609.2.1 = {
	holder = 1020996
}

640.2.1 = {
	holder = 1022957
}

665.2.1 = {
	holder = 1023742
}

665.2.2 = {
	holder = 1024362
}

800.2.1 = {
	holder = 1027581
}

800.2.2 = {
	holder = 1029173
}

814.2.1 = {
	holder = 1031239
}

849.2.1 = {
	holder = 1031971
}

853.2.1 = {
	holder = 1033449
}

864.2.1 = {
	holder = 1033632
}

888.2.1 = {
	holder = 1033778
}

900.2.1 = {
	holder = 1035697
}

1000.2.1 = {
	holder = 1040042
}

1003.2.1 = {
	holder = 1039167
}

1005.2.1 = {
	holder = 1040518
}

1010.2.1 = {
	holder = 1040909
}

1055.2.1 = {
	holder = 1040952
}

1056.2.1 = {
	holder = 1042896
}

