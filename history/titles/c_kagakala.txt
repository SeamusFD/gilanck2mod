200.2.1 = {
	holder = 1000017
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000018
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	holder = 1001220
	law = succ_open_elective
	law = cognatic_succession
}

200.2.4 = {
	liege = d_tasius
}

200.2.5 = {
	holder = 1001221
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.6 = {
	liege = d_buigerus
}

200.2.7 = {
	holder = 1001231
	law = succ_open_elective
	law = cognatic_succession
}

217.2.1 = {
	holder = 1002837
}

257.2.1 = {
	holder = 1004831
}

273.2.1 = {
	holder = 1003206
}

274.2.1 = {
	holder = 1007622
}

283.2.1 = {
	holder = 1008584
}

296.2.1 = {
	holder = 1009035
}

343.2.1 = {
	holder = 1012649
}

346.2.1 = {
	holder = 1010021
}

358.2.1 = {
	holder = 1013625
}

385.2.1 = {
	holder = 1014119
}

407.2.1 = {
	holder = 1016211
}

407.2.2 = {
	holder = 1016884
}

460.2.1 = {
	holder = 1017803
}

469.2.1 = {
	holder = 1020558
}

507.2.1 = {
	holder = 1022886
}

508.2.1 = {
	holder = 1023505
}

543.2.1 = {
	holder = 1024497
}

579.2.1 = {
	holder = 1026387
}

605.2.1 = {
	holder = 1028573
}

653.2.1 = {
	holder = 1032361
}

664.2.1 = {
	holder = 1031440
}

664.2.2 = {
	holder = 1033634
}

675.2.1 = {
	holder = 1033716
}

718.2.1 = {
	holder = 1035183
}

747.2.1 = {
	holder = 1039028
}

761.2.1 = {
	holder = 1039917
}

798.2.1 = {
	holder = 1040453
}

805.2.1 = {
	holder = 1042376
}

857.2.1 = {
	holder = 1045122
}

891.2.1 = {
	holder = 1047755
}

