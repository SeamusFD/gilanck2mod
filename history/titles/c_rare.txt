200.2.1 = {
	holder = 1000039
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1001285
	law = succ_feudal_elective
	law = cognatic_succession
}

200.2.3 = {
	liege = d_nuherru
}

200.2.4 = {
	holder = 1001286
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.5 = {
	holder = 1001283
	law = succ_feudal_elective
	law = cognatic_succession
}

204.2.1 = {
	holder = 1001885
}

280.2.1 = {
	holder = 1004024
}

281.2.1 = {
	holder = 1003676
}

286.2.1 = {
	holder = 1006078
}

331.2.1 = {
	holder = 1006929
}

342.2.1 = {
	holder = 1008638
}

371.2.1 = {
	holder = 1009935
}

376.2.1 = {
	holder = 1010051
}

397.2.1 = {
	holder = 1011488
}

433.2.1 = {
	holder = 1011498
}

439.2.1 = {
	holder = 1011017
}

444.2.1 = {
	holder = 1013426
}

492.2.1 = {
	holder = 1016076
}

542.2.1 = {
	holder = 1016398
}

563.2.1 = {
	holder = 1018895
}

564.2.1 = {
	holder = 1018144
}

564.2.2 = {
	holder = 1019599
}

565.2.1 = {
	holder = 1017924
}

573.2.1 = {
	holder = 1020024
}

608.2.1 = {
	holder = 1020598
}

648.2.1 = {
	holder = 1021970
}

670.2.1 = {
	holder = 1024087
}

720.2.1 = {
	holder = 1024975
}

743.2.1 = {
	holder = 1027482
}

780.2.1 = {
	holder = 1028970
}

818.2.1 = {
	holder = 1030035
}

849.2.1 = {
	holder = 1032235
}

890.2.1 = {
	holder = 1033536
}

930.2.1 = {
	holder = 1035852
}

950.2.1 = {
	holder = 1037162
}

973.2.1 = {
	holder = 1038201
}

1014.2.1 = {
	holder = 1039906
}

1019.2.1 = {
	holder = 1041259
}

1045.2.1 = {
	holder = 1042039
}

