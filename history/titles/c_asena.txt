200.2.1 = {
	holder = 1000020
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000721
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	liege = d_batica
}

200.2.4 = {
	holder = 1000719
	law = succ_primogeniture
	law = cognatic_succession
}

234.2.1 = {
	holder = 1002397
}

239.2.1 = {
	holder = 1003823
}

243.2.1 = {
	holder = 1004096
}

248.2.1 = {
	holder = 1004251
}

303.2.1 = {
	holder = 1005124
}

324.2.1 = {
	holder = 1006198
}

324.2.2 = {
	holder = 1007974
}

363.2.1 = {
	holder = 1007885
}

389.2.1 = {
	holder = 1009745
}

426.2.1 = {
	holder = 1011380
}

429.2.1 = {
	holder = 1012543
}

429.2.2 = {
	holder = 1013070
}

449.2.1 = {
	holder = 1014055
}

472.2.1 = {
	holder = 1012419
}

472.2.2 = {
	holder = 1015069
}

490.2.1 = {
	holder = 1015970
}

497.2.1 = {
	holder = 1016189
}

501.2.1 = {
	holder = 1015180
}

521.2.1 = {
	holder = 1017473
}

561.2.1 = {
	holder = 1017713
}

579.2.1 = {
	holder = 1019465
}

619.2.1 = {
	holder = 1020587
}

675.2.1 = {
	holder = 1022316
}

675.2.2 = {
	holder = 1024829
}

713.2.1 = {
	holder = 1024908
}

721.2.1 = {
	holder = 1026549
}

722.2.1 = {
	holder = 1027161
}

765.2.1 = {
	holder = 1027761
}

793.2.1 = {
	holder = 1029871
}

837.2.1 = {
	holder = 1031874
}

881.2.1 = {
	holder = 1032775
}

900.2.1 = {
	holder = 1034910
}

931.2.1 = {
	holder = 1035966
}

937.2.1 = {
	holder = 1037256
}

947.2.1 = {
	holder = 1035589
}

962.2.1 = {
	holder = 1037466
}

962.2.2 = {
	holder = 1038560
}

983.2.1 = {
	holder = 1039539
}

993.2.1 = {
	holder = 1040028
}

1018.2.1 = {
	holder = 1040068
}

1051.2.1 = {
	holder = 1041605
}

