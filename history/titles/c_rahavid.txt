200.2.1 = {
	holder = 1000011
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000851
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.3 = {
	liege = d_kav
}

200.2.4 = {
	holder = 1000852
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.5 = {
	holder = 1000849
	law = succ_primogeniture
	law = agnatic_succession
}

209.2.1 = {
	holder = 1002667
}

215.2.1 = {
	holder = 1000821
}

224.2.1 = {
	holder = 1004539
}

250.2.1 = {
	holder = 1005983
}

295.2.1 = {
	holder = 1006366
}

310.2.1 = {
	holder = 1010417
}

333.2.1 = {
	holder = 1010419
}

333.2.2 = {
	holder = 1011971
}

350.2.1 = {
	holder = 1012528
}

369.2.1 = {
	holder = 1013867
}

403.2.1 = {
	holder = 1014593
}

404.2.1 = {
	holder = 1016627
}

500.2.1 = {
	holder = 1021516
}

543.2.1 = {
	holder = 1022500
}

555.2.1 = {
	holder = 1026486
}

581.2.1 = {
	holder = 1027394
}

597.2.1 = {
	holder = 1029173
}

647.2.1 = {
	holder = 1029545
}

647.2.2 = {
	holder = 1032507
}

664.2.1 = {
	holder = 1032804
}

675.2.1 = {
	holder = 1032060
}

675.2.2 = {
	holder = 1029591
}

678.2.1 = {
	holder = 1032735
}

704.2.1 = {
	holder = 1033899
}

708.2.1 = {
	holder = 1036575
}

745.2.1 = {
	holder = 1036668
}

753.2.1 = {
	holder = 1039051
}

786.2.1 = {
	holder = 1040992
}

805.2.1 = {
	holder = 1041982
}

866.2.1 = {
	holder = 1046204
}

