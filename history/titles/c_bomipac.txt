200.2.1 = {
	holder = 1000005
	law = succ_tanistry
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000006
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	liege = d_nacedda
}

200.2.4 = {
	liege = d_rermun
}

200.2.5 = {
	holder = 1000592
	law = succ_primogeniture
	law = agnatic_succession
}

212.2.1 = {
	holder = 1003466
}

238.2.1 = {
	holder = 1004933
}

294.2.1 = {
	holder = 1009307
}

307.2.1 = {
	holder = 1009392
}

317.2.1 = {
	holder = 1006874
}

317.2.2 = {
	holder = 1010952
}

329.2.1 = {
	holder = 1011752
}

341.2.1 = {
	holder = 1012492
}

348.2.1 = {
	holder = 1012982
}

359.2.1 = {
	holder = 1013481
}

360.2.1 = {
	holder = 1013769
}

395.2.1 = {
	holder = 1014179
}

409.2.1 = {
	holder = 1016225
}

441.2.1 = {
	holder = 1014768
}

447.2.1 = {
	holder = 1016225
}

447.2.2 = {
	holder = 1014176
}

449.2.1 = {
	holder = 1015843
}

450.2.1 = {
	holder = 1019764
}

470.2.1 = {
	holder = 1021085
}

509.2.1 = {
	holder = 1021686
}

557.2.1 = {
	holder = 1023608
}

561.2.1 = {
	holder = 1024527
}

561.2.2 = {
	holder = 1024141
}

571.2.1 = {
	holder = 1027022
}

583.2.1 = {
	holder = 1028294
}

592.2.1 = {
	holder = 1027934
}

592.2.2 = {
	holder = 1028894
}

599.2.1 = {
	holder = 1027934
}

626.2.1 = {
	holder = 1029085
}

635.2.1 = {
	holder = 1029276
}

638.2.1 = {
	holder = 1031664
}

651.2.1 = {
	holder = 1032775
}

677.2.1 = {
	holder = 1033103
}

718.2.1 = {
	holder = 1034558
}

719.2.1 = {
	holder = 1037225
}

750.2.1 = {
	holder = 1037667
}

764.2.1 = {
	holder = 1039475
}

769.2.1 = {
	holder = 1038339
}

769.2.2 = {
	holder = 1040486
}

900.2.1 = {
	holder = 1048475
}

