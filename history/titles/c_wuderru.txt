200.2.1 = {
	holder = 1000039
	law = succ_feudal_elective
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000040
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1001316
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.4 = {
	liege = d_etugano
}

200.2.5 = {
	holder = 1001315
	law = succ_elective_gavelkind
	law = agnatic_succession
}

202.2.1 = {
	holder = 1001407
}

208.2.1 = {
	holder = 1002302
}

209.2.1 = {
	holder = 1002487
}

229.2.1 = {
	holder = 1002848
}

273.2.1 = {
	holder = 1004098
}

279.2.1 = {
	holder = 1005778
}

284.2.1 = {
	holder = 1004655
}

285.2.1 = {
	holder = 1006086
}

293.2.1 = {
	holder = 1006351
}

310.2.1 = {
	holder = 1007196
}

317.2.1 = {
	holder = 1006888
}

317.2.2 = {
	holder = 1007618
}

327.2.1 = {
	holder = 1007660
}

333.2.1 = {
	holder = 1006888
}

333.2.2 = {
	holder = 1006551
}

400.2.1 = {
	holder = 1009952
}

400.2.2 = {
	holder = 1011685
}

500.2.1 = {
	holder = 1014938
}

533.2.1 = {
	holder = 1016927
}

566.2.1 = {
	holder = 1018153
}

574.2.1 = {
	holder = 1019774
}

629.2.1 = {
	holder = 1021575
}

630.2.1 = {
	holder = 1022735
}

656.2.1 = {
	holder = 1023416
}

684.2.1 = {
	holder = 1025283
}

710.2.1 = {
	holder = 1025486
}

741.2.1 = {
	holder = 1026611
}

766.2.1 = {
	holder = 1028074
}

801.2.1 = {
	holder = 1029688
}

801.2.2 = {
	holder = 1030974
}

900.2.1 = {
	holder = 1035426
}

924.2.1 = {
	holder = 1036113
}

950.2.1 = {
	holder = 1037268
}

980.2.1 = {
	holder = 1038051
}

1020.2.1 = {
	holder = 1039745
}

1030.2.1 = {
	holder = 1041735
}

1055.2.1 = {
	holder = 1042851
}

1058.2.1 = {
	holder = 1043217
}

