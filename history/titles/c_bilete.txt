200.2.1 = {
	holder = 1000005
	law = succ_tanistry
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000019
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	holder = 1001264
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.4 = {
	liege = d_awurati
}

200.2.5 = {
	holder = 1001263
	law = succ_primogeniture
	law = agnatic_succession
}

217.2.1 = {
	holder = 1002333
}

227.2.1 = {
	holder = 1004202
}

232.2.1 = {
	holder = 1001246
}

237.2.1 = {
	holder = 1005354
}

290.2.1 = {
	holder = 1005611
}

309.2.1 = {
	holder = 1009068
}

324.2.1 = {
	holder = 1009413
}

349.2.1 = {
	holder = 1013021
}

364.2.1 = {
	holder = 1013536
}

400.2.1 = {
	holder = 1015311
}

412.2.1 = {
	holder = 1016425
}

449.2.1 = {
	holder = 1019739
}

489.2.1 = {
	holder = 1020304
}

510.2.1 = {
	holder = 1022680
}

514.2.1 = {
	holder = 1023681
}

546.2.1 = {
	holder = 1025968
}

558.2.1 = {
	holder = 1023624
}

558.2.2 = {
	holder = 1026711
}

564.2.1 = {
	holder = 1026145
}

565.2.1 = {
	holder = 1027125
}

605.2.1 = {
	holder = 1027210
}

636.2.1 = {
	holder = 1031564
}

690.2.1 = {
	holder = 1033122
}

710.2.1 = {
	holder = 1035338
}

771.2.1 = {
	holder = 1036754
}

780.2.1 = {
	holder = 1040749
}

805.2.1 = {
	holder = 1041332
}

806.2.1 = {
	holder = 1042772
}

900.2.1 = {
	holder = 1048350
}

