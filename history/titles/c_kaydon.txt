200.2.1 = {
	holder = 1000034
	law = succ_elective_gavelkind
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000035
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	holder = 1001761
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.4 = {
	liege = d_nulika
}

200.2.5 = {
	holder = 1001760
	law = succ_primogeniture
	law = cognatic_succession
}

239.2.1 = {
	holder = 1004156
}

240.2.1 = {
	holder = 1003516
}

244.2.1 = {
	holder = 1003125
}

244.2.2 = {
	holder = 1005834
}

400.2.1 = {
	holder = 1015693
}

408.2.1 = {
	holder = 1016021
}

428.2.1 = {
	holder = 1018026
}

429.2.1 = {
	holder = 1017153
}

446.2.1 = {
	holder = 1019570
}

600.2.1 = {
	holder = 1024251
}

700.2.1 = {
	holder = 1032618
}

701.2.1 = {
	holder = 1035620
}

743.2.1 = {
	holder = 1036491
}

743.2.2 = {
	holder = 1038778
}

900.2.1 = {
	holder = 1048393
}

