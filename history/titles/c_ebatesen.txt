200.2.1 = {
	holder = 1000005
	law = succ_tanistry
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000019
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	liege = d_dagechok
}

200.2.4 = {
	liege = d_dagechok
}

200.2.5 = {
	holder = 1001246
	law = succ_primogeniture
	law = agnatic_succession
}

223.2.1 = {
	holder = 1004471
}

270.2.1 = {
	holder = 1005192
}

273.2.1 = {
	holder = 1007813
}

326.2.1 = {
	holder = 1009205
}

344.2.1 = {
	holder = 1011658
}

347.2.1 = {
	holder = 1009413
}

347.2.2 = {
	holder = 1012894
}

349.2.1 = {
	holder = 1009413
}

349.2.2 = {
	holder = 1013022
}

351.2.1 = {
	holder = 1011933
}

386.2.1 = {
	holder = 1013296
}

405.2.1 = {
	holder = 1015562
}

434.2.1 = {
	holder = 1016858
}

443.2.1 = {
	holder = 1019220
}

450.2.1 = {
	holder = 1019790
}

454.2.1 = {
	holder = 1019935
}

460.2.1 = {
	holder = 1019739
}

489.2.1 = {
	holder = 1020304
}

506.2.1 = {
	holder = 1023346
}

510.2.1 = {
	holder = 1022680
}

514.2.1 = {
	holder = 1023681
}

515.2.1 = {
	holder = 1023930
}

532.2.1 = {
	holder = 1024649
}

536.2.1 = {
	holder = 1023369
}

550.2.1 = {
	holder = 1026240
}

574.2.1 = {
	holder = 1027209
}

578.2.1 = {
	holder = 1027472
}

578.2.2 = {
	holder = 1022946
}

579.2.1 = {
	holder = 1026024
}

600.2.1 = {
	holder = 1029386
}

602.2.1 = {
	holder = 1029551
}

648.2.1 = {
	holder = 1032539
}

657.2.1 = {
	holder = 1033118
}

712.2.1 = {
	holder = 1033718
}

722.2.1 = {
	holder = 1036784
}

722.2.2 = {
	holder = 1037462
}

725.2.1 = {
	holder = 1037631
}

777.2.1 = {
	holder = 1038637
}

779.2.1 = {
	holder = 1038374
}

804.2.1 = {
	holder = 1042666
}

900.2.1 = {
	holder = 1045858
}

900.2.2 = {
	holder = 1048944
}

