200.2.1 = {
	holder = 1000034
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1001059
	law = succ_feudal_elective
	law = cognatic_succession
}

200.2.3 = {
	liege = d_kililde
}

200.2.4 = {
	holder = 1001057
	law = succ_feudal_elective
	law = cognatic_succession
}

230.2.1 = {
	holder = 1001966
}

235.2.1 = {
	holder = 1003498
}

270.2.1 = {
	holder = 1004566
}

305.2.1 = {
	holder = 1005489
}

314.2.1 = {
	holder = 1005992
}

321.2.1 = {
	holder = 1007440
}

322.2.1 = {
	holder = 1007014
}

322.2.2 = {
	holder = 1007849
}

361.2.1 = {
	holder = 1008063
}

391.2.1 = {
	holder = 1009883
}

400.2.1 = {
	holder = 1011592
}

460.2.1 = {
	holder = 1012916
}

461.2.1 = {
	holder = 1013007
}

462.2.1 = {
	holder = 1014621
}

518.2.1 = {
	holder = 1016532
}

572.2.1 = {
	holder = 1017537
}

579.2.1 = {
	holder = 1017535
}

580.2.1 = {
	holder = 1020337
}

588.2.1 = {
	holder = 1019337
}

589.2.1 = {
	holder = 1020786
}

629.2.1 = {
	holder = 1021077
}

639.2.1 = {
	holder = 1022722
}

643.2.1 = {
	holder = 1023359
}

648.2.1 = {
	holder = 1022788
}

653.2.1 = {
	holder = 1023774
}

683.2.1 = {
	holder = 1023927
}

684.2.1 = {
	holder = 1023092
}

690.2.1 = {
	holder = 1024719
}

692.2.1 = {
	holder = 1025670
}

701.2.1 = {
	holder = 1024719
}

702.2.1 = {
	holder = 1026174
}

751.2.1 = {
	holder = 1026232
}

759.2.1 = {
	holder = 1028674
}

787.2.1 = {
	holder = 1030208
}

799.2.1 = {
	holder = 1029569
}

799.2.2 = {
	holder = 1030844
}

840.2.1 = {
	holder = 1030955
}

878.2.1 = {
	holder = 1033529
}

912.2.1 = {
	holder = 1034874
}

915.2.1 = {
	holder = 1034286
}

916.2.1 = {
	holder = 1036409
}

941.2.1 = {
	holder = 1036558
}

974.2.1 = {
	holder = 1037704
}

1005.2.1 = {
	holder = 1039340
}

1032.2.1 = {
	holder = 1041249
}

1045.2.1 = {
	holder = 1042224
}

