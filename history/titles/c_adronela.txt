200.2.1 = {
	holder = 1000022
	law = succ_tanistry
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000023
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	holder = 1001389
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.4 = {
	liege = d_ewanaac
}

200.2.5 = {
	holder = 1001387
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.6 = {
	holder = 1001371
}

201.2.1 = {
	holder = 1002704
}

300.2.1 = {
	holder = 1007020
}

304.2.1 = {
	holder = 1009419
}

305.2.1 = {
	holder = 1008324
}

306.2.1 = {
	holder = 1010152
}

400.2.1 = {
	holder = 1014345
}

421.2.1 = {
	holder = 1016861
}

437.2.1 = {
	holder = 1015888
}

437.2.2 = {
	holder = 1018947
}

477.2.1 = {
	holder = 1019360
}

511.2.1 = {
	holder = 1022407
}

540.2.1 = {
	holder = 1023741
}

546.2.1 = {
	holder = 1025644
}

582.2.1 = {
	holder = 1028121
}

583.2.1 = {
	holder = 1027142
}

608.2.1 = {
	holder = 1029944
}

624.2.1 = {
	holder = 1030403
}

639.2.1 = {
	holder = 1031293
}

677.2.1 = {
	holder = 1031685
}

695.2.1 = {
	holder = 1035006
}

719.2.1 = {
	holder = 1036610
}

769.2.1 = {
	holder = 1038258
}

796.2.1 = {
	holder = 1041046
}

800.2.1 = {
	holder = 1042382
}

900.2.1 = {
	holder = 1047383
}

