200.2.1 = {
	holder = 1000017
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.2 = {
	holder = 1001155
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.3 = {
	liege = d_kaxozar
}

200.2.4 = {
	holder = 1001156
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.5 = {
	liege = d_kudozar
}

200.2.6 = {
	holder = 1001185
	law = succ_primogeniture
	law = agnatic_succession
}

245.2.1 = {
	holder = 1002689
}

262.2.1 = {
	holder = 1006468
}

263.2.1 = {
	holder = 1006075
}

263.2.2 = {
	holder = 1007199
}

304.2.1 = {
	holder = 1007595
}

320.2.1 = {
	holder = 1007249
}

320.2.2 = {
	holder = 1011153
}

328.2.1 = {
	holder = 1010069
}

370.2.1 = {
	holder = 1012239
}

411.2.1 = {
	holder = 1017146
}

428.2.1 = {
	holder = 1017545
}

472.2.1 = {
	holder = 1020815
}

515.2.1 = {
	holder = 1023678
}

516.2.1 = {
	holder = 1023987
}

546.2.1 = {
	holder = 1025076
}

547.2.1 = {
	holder = 1026021
}

548.2.1 = {
	holder = 1026097
}

592.2.1 = {
	holder = 1026495
}

610.2.1 = {
	holder = 1029549
}

618.2.1 = {
	holder = 1029206
}

619.2.1 = {
	holder = 1026213
}

634.2.1 = {
	holder = 1029484
}

636.2.1 = {
	holder = 1029520
}

659.2.1 = {
	holder = 1032247
}

670.2.1 = {
	holder = 1033571
}

697.2.1 = {
	holder = 1030347
}

698.2.1 = {
	holder = 1035829
}

745.2.1 = {
	holder = 1037138
}

798.2.1 = {
	holder = 1039429
}

805.2.1 = {
	holder = 1042533
}

845.2.1 = {
	holder = 1045054
}

849.2.1 = {
	holder = 1043011
}

850.2.1 = {
	holder = 1045688
}

866.2.1 = {
	holder = 1044225
}

875.2.1 = {
	holder = 1047078
}

886.2.1 = {
	holder = 1046759
}

886.2.2 = {
	holder = 1047978
}

