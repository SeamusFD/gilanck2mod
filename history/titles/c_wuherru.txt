200.2.1 = {
	holder = 1000039
	law = succ_feudal_elective
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000040
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1001340
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.4 = {
	liege = d_sanict
}

200.2.5 = {
	holder = 1001339
	law = succ_elective_gavelkind
	law = agnatic_succession
}

224.2.1 = {
	holder = 1002361
}

238.2.1 = {
	holder = 1003245
}

257.2.1 = {
	holder = 1004030
}

288.2.1 = {
	holder = 1005142
}

339.2.1 = {
	holder = 1006689
}

353.2.1 = {
	holder = 1008772
}

375.2.1 = {
	holder = 1009519
}

413.2.1 = {
	holder = 1011025
}

456.2.1 = {
	holder = 1013848
}

462.2.1 = {
	holder = 1014045
}

480.2.1 = {
	holder = 1014934
}

516.2.1 = {
	holder = 1015749
}

525.2.1 = {
	holder = 1017381
}

531.2.1 = {
	holder = 1015494
}

531.2.2 = {
	holder = 1017985
}

532.2.1 = {
	holder = 1015707
}

532.2.2 = {
	holder = 1018031
}

703.2.1 = {
	holder = 1023551
}

710.2.1 = {
	holder = 1025183
}

745.2.1 = {
	holder = 1026927
}

774.2.1 = {
	holder = 1028628
}

787.2.1 = {
	holder = 1029731
}

839.2.1 = {
	holder = 1032601
}

881.2.1 = {
	holder = 1033590
}

899.2.1 = {
	holder = 1035004
}

900.2.1 = {
	holder = 1035703
}

1000.2.1 = {
	holder = 1038301
}

1006.2.1 = {
	holder = 1040524
}

1008.2.1 = {
	holder = 1040171
}

1009.2.1 = {
	holder = 1039287
}

1010.2.1 = {
	holder = 1040171
}

1011.2.1 = {
	holder = 1040947
}

1048.2.1 = {
	holder = 1040955
}

1063.2.1 = {
	holder = 1041717
}

