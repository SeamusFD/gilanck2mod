200.2.1 = {
	holder = 1000037
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000038
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1001206
	law = succ_tanistry
	law = cognatic_succession
}

200.2.4 = {
	liege = d_neirilas
}

200.2.5 = {
	holder = 1001205
	law = succ_tanistry
	law = cognatic_succession
}

235.2.1 = {
	holder = 1002417
}

248.2.1 = {
	holder = 1001693
}

261.2.1 = {
	holder = 1004475
}

321.2.1 = {
	holder = 1005318
}

322.2.1 = {
	holder = 1007853
}

500.2.1 = {
	holder = 1013426
}

501.2.1 = {
	holder = 1014740
}

507.2.1 = {
	holder = 1015489
}

507.2.2 = {
	holder = 1016824
}

543.2.1 = {
	holder = 1017067
}

573.2.1 = {
	holder = 1018845
}

582.2.1 = {
	holder = 1020444
}

583.2.1 = {
	holder = 1018146
}

584.2.1 = {
	holder = 1020549
}

604.2.1 = {
	holder = 1021482
}

627.2.1 = {
	holder = 1022008
}

643.2.1 = {
	holder = 1022906
}

699.2.1 = {
	holder = 1023885
}

709.2.1 = {
	holder = 1026295
}

712.2.1 = {
	holder = 1025623
}

713.2.1 = {
	holder = 1026696
}

735.2.1 = {
	holder = 1027349
}

737.2.1 = {
	holder = 1027447
}

774.2.1 = {
	holder = 1027480
}

781.2.1 = {
	holder = 1030023
}

791.2.1 = {
	holder = 1030029
}

801.2.1 = {
	holder = 1030901
}

810.2.1 = {
	holder = 1030760
}

810.2.2 = {
	holder = 1031457
}

828.2.1 = {
	holder = 1031630
}

852.2.1 = {
	holder = 1032464
}

877.2.1 = {
	holder = 1033587
}

893.2.1 = {
	holder = 1034827
}

901.2.1 = {
	holder = 1035560
}

937.2.1 = {
	holder = 1036420
}

975.2.1 = {
	holder = 1037797
}

976.2.1 = {
	holder = 1038091
}

977.2.1 = {
	holder = 1039288
}

1018.2.1 = {
	holder = 1039503
}

1040.2.1 = {
	holder = 1041394
}

1064.2.1 = {
	holder = 1043507
}

