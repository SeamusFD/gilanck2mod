200.2.1 = {
	holder = 1000032
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000033
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1001010
	law = succ_open_elective
	law = cognatic_succession
}

200.2.4 = {
	liege = d_edogodai
}

200.2.5 = {
	holder = 1001008
	law = succ_open_elective
	law = cognatic_succession
}

235.2.1 = {
	holder = 1002052
}

266.2.1 = {
	holder = 1004171
}

298.2.1 = {
	holder = 1005600
}

303.2.1 = {
	holder = 1006486
}

303.2.2 = {
	holder = 1006920
}

356.2.1 = {
	holder = 1008411
}

364.2.1 = {
	holder = 1009663
}

384.2.1 = {
	holder = 1010528
}

399.2.1 = {
	holder = 1009344
}

399.2.2 = {
	holder = 1011589
}

408.2.1 = {
	holder = 1011648
}

409.2.1 = {
	holder = 1011861
}

412.2.1 = {
	holder = 1011907
}

446.2.1 = {
	holder = 1013883
}

484.2.1 = {
	holder = 1014150
}

502.2.1 = {
	holder = 1016153
}

510.2.1 = {
	holder = 1016887
}

544.2.1 = {
	holder = 1016960
}

551.2.1 = {
	holder = 1018979
}

575.2.1 = {
	holder = 1019380
}

593.2.1 = {
	holder = 1020699
}

619.2.1 = {
	holder = 1021437
}

625.2.1 = {
	holder = 1020290
}

625.2.2 = {
	holder = 1022468
}

626.2.1 = {
	holder = 1021999
}

626.2.2 = {
	holder = 1022523
}

800.2.1 = {
	holder = 1025669
}

800.2.2 = {
	holder = 1028720
}

815.2.1 = {
	holder = 1029662
}

824.2.1 = {
	holder = 1032145
}

830.2.1 = {
	holder = 1029935
}

837.2.1 = {
	holder = 1032046
}

838.2.1 = {
	holder = 1032781
}

863.2.1 = {
	holder = 1033081
}

905.2.1 = {
	holder = 1035240
}

960.2.1 = {
	holder = 1036058
}

975.2.1 = {
	holder = 1038617
}

1028.2.1 = {
	holder = 1039690
}

1041.2.1 = {
	holder = 1042079
}

1062.2.1 = {
	holder = 1043071
}

