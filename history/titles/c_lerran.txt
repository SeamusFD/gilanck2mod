200.2.1 = {
	holder = 1000039
	law = succ_feudal_elective
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000040
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1001345
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.4 = {
	liege = d_koret
}

200.2.5 = {
	holder = 1001343
	law = succ_elective_gavelkind
	law = agnatic_succession
}

214.2.1 = {
	holder = 1002146
}

219.2.1 = {
	holder = 1001252
}

219.2.2 = {
	holder = 1002229
}

233.2.1 = {
	holder = 1003254
}

252.2.1 = {
	holder = 1004141
}

270.2.1 = {
	holder = 1004945
}

297.2.1 = {
	holder = 1005835
}

301.2.1 = {
	holder = 1006835
}

317.2.1 = {
	holder = 1004840
}

319.2.1 = {
	holder = 1005724
}

332.2.1 = {
	holder = 1008080
}

360.2.1 = {
	holder = 1008865
}

397.2.1 = {
	holder = 1008936
}

408.2.1 = {
	holder = 1011022
}

420.2.1 = {
	holder = 1011918
}

454.2.1 = {
	holder = 1013349
}

468.2.1 = {
	holder = 1012027
}

471.2.1 = {
	holder = 1013013
}

487.2.1 = {
	holder = 1015849
}

517.2.1 = {
	holder = 1016033
}

524.2.1 = {
	holder = 1017386
}

530.2.1 = {
	holder = 1017930
}

567.2.1 = {
	holder = 1018790
}

593.2.1 = {
	holder = 1020037
}

628.2.1 = {
	holder = 1021036
}

670.2.1 = {
	holder = 1022960
}

678.2.1 = {
	holder = 1024776
}

684.2.1 = {
	holder = 1023977
}

718.2.1 = {
	holder = 1025683
}

749.2.1 = {
	holder = 1027170
}

784.2.1 = {
	holder = 1028472
}

803.2.1 = {
	holder = 1028788
}

803.2.2 = {
	holder = 1031073
}

829.2.1 = {
	holder = 1031925
}

833.2.1 = {
	holder = 1030971
}

842.2.1 = {
	holder = 1031130
}

874.2.1 = {
	holder = 1033454
}

875.2.1 = {
	holder = 1034510
}

915.2.1 = {
	holder = 1035202
}

916.2.1 = {
	holder = 1036416
}

924.2.1 = {
	holder = 1036486
}

935.2.1 = {
	holder = 1037211
}

943.2.1 = {
	holder = 1037069
}

944.2.1 = {
	holder = 1037749
}

944.2.2 = {
	holder = 1037073
}

964.2.1 = {
	holder = 1038680
}

1011.2.1 = {
	holder = 1039293
}

1035.2.1 = {
	holder = 1041093
}

1045.2.1 = {
	holder = 1042241
}

