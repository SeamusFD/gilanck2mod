200.2.1 = {
	holder = 1000013
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000360
	law = succ_gavelkind
	law = cognatic_succession
}

200.2.3 = {
	liege = d_kerin
}

200.2.4 = {
	holder = 1000361
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.5 = {
	holder = 1000358
	law = succ_gavelkind
	law = cognatic_succession
}

201.2.1 = {
	holder = 1000383
}

201.2.2 = {
	holder = 1001824
}

300.2.1 = {
	holder = 1006037
}

300.2.2 = {
	holder = 1006776
}

314.2.1 = {
	holder = 1006037
}

314.2.2 = {
	holder = 1007457
}

400.2.1 = {
	holder = 1011608
}

500.2.1 = {
	holder = 1015557
}

525.2.1 = {
	holder = 1016312
}

526.2.1 = {
	holder = 1014365
}

600.2.1 = {
	holder = 1019547
}

610.2.1 = {
	holder = 1020187
}

642.2.1 = {
	holder = 1022496
}

643.2.1 = {
	holder = 1021450
}

643.2.2 = {
	holder = 1023370
}

800.2.1 = {
	holder = 1029736
}

821.2.1 = {
	holder = 1030049
}

847.2.1 = {
	holder = 1032169
}

859.2.1 = {
	holder = 1033278
}

891.2.1 = {
	holder = 1034271
}

914.2.1 = {
	holder = 1035535
}

934.2.1 = {
	holder = 1036446
}

952.2.1 = {
	holder = 1037392
}

963.2.1 = {
	holder = 1038628
}

967.2.1 = {
	holder = 1038648
}

989.2.1 = {
	holder = 1039431
}

995.2.1 = {
	holder = 1040117
}

1023.2.1 = {
	holder = 1041537
}

1061.2.1 = {
	holder = 1041823
}

