200.2.1 = {
	holder = 1000004
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000239
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	liege = d_begilia
}

200.2.4 = {
	holder = 1000240
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.5 = {
	holder = 1000237
	law = succ_elective_gavelkind
	law = agnatic_succession
}

210.2.1 = {
	holder = 1002093
}

214.2.1 = {
	holder = 1000253
}

229.2.1 = {
	holder = 1001923
}

244.2.1 = {
	holder = 1003484
}

249.2.1 = {
	holder = 1004351
}

286.2.1 = {
	holder = 1006087
}

328.2.1 = {
	holder = 1007223
}

339.2.1 = {
	holder = 1008195
}

341.2.1 = {
	holder = 1006207
}

348.2.1 = {
	holder = 1007270
}

382.2.1 = {
	holder = 1009151
}

388.2.1 = {
	holder = 1009426
}

415.2.1 = {
	holder = 1011174
}

425.2.1 = {
	holder = 1012549
}

450.2.1 = {
	holder = 1013699
}

472.2.1 = {
	holder = 1014957
}

523.2.1 = {
	holder = 1015371
}

524.2.1 = {
	holder = 1017611
}

544.2.1 = {
	holder = 1017756
}

611.2.1 = {
	holder = 1019244
}

624.2.1 = {
	holder = 1022408
}

665.2.1 = {
	holder = 1022492
}

672.2.1 = {
	holder = 1024428
}

684.2.1 = {
	holder = 1022800
}

693.2.1 = {
	holder = 1024547
}

704.2.1 = {
	holder = 1026262
}

735.2.1 = {
	holder = 1025935
}

736.2.1 = {
	holder = 1027751
}

756.2.1 = {
	holder = 1027938
}

769.2.1 = {
	holder = 1029188
}

775.2.1 = {
	holder = 1029189
}

776.2.1 = {
	holder = 1029749
}

786.2.1 = {
	holder = 1029995
}

787.2.1 = {
	holder = 1029440
}

800.2.1 = {
	holder = 1030859
}

809.2.1 = {
	holder = 1029903
}

810.2.1 = {
	holder = 1031417
}

822.2.1 = {
	holder = 1031592
}

823.2.1 = {
	holder = 1032071
}

870.2.1 = {
	holder = 1032473
}

871.2.1 = {
	holder = 1034314
}

894.2.1 = {
	holder = 1034675
}

919.2.1 = {
	holder = 1036243
}

920.2.1 = {
	holder = 1034228
}

924.2.1 = {
	holder = 1034678
}

927.2.1 = {
	holder = 1035109
}

953.2.1 = {
	holder = 1037231
}

1000.2.1 = {
	holder = 1038500
}

1003.2.1 = {
	holder = 1037387
}

1003.2.2 = {
	holder = 1040181
}

1003.2.3 = {
	holder = 1038794
}

1024.2.1 = {
	holder = 1040631
}

1040.2.1 = {
	holder = 1041810
}

1043.2.1 = {
	holder = 1041330
}

1044.2.1 = {
	holder = 1042540
}

