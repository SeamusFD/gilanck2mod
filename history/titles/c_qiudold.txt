200.2.1 = {
	holder = 1000015
	law = succ_seniority
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000449
	law = succ_seniority
	law = cognatic_succession
}

200.2.3 = {
	liege = d_supold
}

200.2.4 = {
	holder = 1000450
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.5 = {
	holder = 1000447
	law = succ_seniority
	law = cognatic_succession
}

228.2.1 = {
	holder = 1002689
}

246.2.1 = {
	holder = 1004198
}

257.2.1 = {
	holder = 1002689
}

258.2.1 = {
	holder = 1004773
}

280.2.1 = {
	holder = 1004904
}

312.2.1 = {
	holder = 1005853
}

322.2.1 = {
	holder = 1007530
}

355.2.1 = {
	holder = 1009357
}

396.2.1 = {
	holder = 1010210
}

403.2.1 = {
	holder = 1010564
}

446.2.1 = {
	holder = 1013864
}

487.2.1 = {
	holder = 1013921
}

503.2.1 = {
	holder = 1016371
}

569.2.1 = {
	holder = 1017315
}

570.2.1 = {
	holder = 1017359
}

571.2.1 = {
	holder = 1019903
}

700.2.1 = {
	holder = 1024705
}

700.2.2 = {
	holder = 1023651
}

701.2.1 = {
	holder = 1026137
}

703.2.1 = {
	holder = 1024786
}

711.2.1 = {
	holder = 1026434
}

714.2.1 = {
	holder = 1026768
}

724.2.1 = {
	holder = 1026947
}

733.2.1 = {
	holder = 1027644
}

734.2.1 = {
	holder = 1026434
}

734.2.2 = {
	holder = 1027707
}

744.2.1 = {
	holder = 1027800
}

747.2.1 = {
	holder = 1026434
}

747.2.2 = {
	holder = 1028326
}

751.2.1 = {
	holder = 1026434
}

751.2.2 = {
	holder = 1028514
}

788.2.1 = {
	holder = 1028852
}

791.2.1 = {
	holder = 1030474
}

842.2.1 = {
	holder = 1031320
}

864.2.1 = {
	holder = 1033025
}

866.2.1 = {
	holder = 1034045
}

884.2.1 = {
	holder = 1034854
}

897.2.1 = {
	holder = 1034768
}

904.2.1 = {
	holder = 1035540
}

932.2.1 = {
	holder = 1036541
}

990.2.1 = {
	holder = 1037398
}

992.2.1 = {
	holder = 1039932
}

1000.2.1 = {
	holder = 1040344
}

