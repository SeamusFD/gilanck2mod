200.2.1 = {
	holder = 1000017
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.2 = {
	holder = 1001123
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.3 = {
	liege = d_nagakala
}

200.2.4 = {
	holder = 1001124
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.5 = {
	holder = 1001121
	law = succ_primogeniture
	law = agnatic_succession
}

203.2.1 = {
	holder = 1002830
}

231.2.1 = {
	holder = 1004198
}

249.2.1 = {
	holder = 1002759
}

249.2.2 = {
	holder = 1006220
}

287.2.1 = {
	holder = 1007382
}

317.2.1 = {
	holder = 1010142
}

346.2.1 = {
	holder = 1012833
}

352.2.1 = {
	holder = 1011069
}

352.2.2 = {
	holder = 1013232
}

500.2.1 = {
	holder = 1018287
}

509.2.1 = {
	holder = 1020748
}

522.2.1 = {
	holder = 1022153
}

525.2.1 = {
	holder = 1023677
}

548.2.1 = {
	holder = 1026096
}

569.2.1 = {
	holder = 1026754
}

614.2.1 = {
	holder = 1027778
}

636.2.1 = {
	holder = 1030396
}

650.2.1 = {
	holder = 1030796
}

650.2.2 = {
	holder = 1032660
}

680.2.1 = {
	holder = 1034166
}

694.2.1 = {
	holder = 1035253
}

702.2.1 = {
	holder = 1033713
}

712.2.1 = {
	holder = 1033501
}

717.2.1 = {
	holder = 1036923
}

727.2.1 = {
	holder = 1036156
}

747.2.1 = {
	holder = 1036831
}

770.2.1 = {
	holder = 1040202
}

800.2.1 = {
	holder = 1042425
}

900.2.1 = {
	holder = 1045624
}

900.2.2 = {
	holder = 1048905
}

