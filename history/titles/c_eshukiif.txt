200.2.1 = {
	holder = 1000037
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1001136
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	liege = d_qabara
}

200.2.4 = {
	holder = 1001135
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.5 = {
	holder = 1001405
}

202.2.1 = {
	holder = 1001252
}

213.2.1 = {
	holder = 1002676
}

215.2.1 = {
	holder = 1001151
}

222.2.1 = {
	holder = 1002518
}

247.2.1 = {
	holder = 1003375
}

265.2.1 = {
	holder = 1002787
}

269.2.1 = {
	holder = 1004840
}

269.2.2 = {
	holder = 1005319
}

329.2.1 = {
	holder = 1005402
}

338.2.1 = {
	holder = 1007909
}

343.2.1 = {
	holder = 1008865
}

361.2.1 = {
	holder = 1009684
}

391.2.1 = {
	holder = 1010936
}

391.2.2 = {
	holder = 1007249
}

401.2.1 = {
	holder = 1011249
}

404.2.1 = {
	holder = 1011022
}

406.2.1 = {
	holder = 1008125
}

417.2.1 = {
	holder = 1012027
}

471.2.1 = {
	holder = 1013013
}

475.2.1 = {
	holder = 1015250
}

476.2.1 = {
	holder = 1014045
}

476.2.2 = {
	holder = 1015297
}

522.2.1 = {
	holder = 1015985
}

526.2.1 = {
	holder = 1017675
}

566.2.1 = {
	holder = 1018503
}

583.2.1 = {
	holder = 1020310
}

583.2.2 = {
	holder = 1020512
}

587.2.1 = {
	holder = 1019219
}

597.2.1 = {
	holder = 1020511
}

597.2.2 = {
	holder = 1021173
}

628.2.1 = {
	holder = 1022638
}

658.2.1 = {
	holder = 1023003
}

670.2.1 = {
	holder = 1022333
}

678.2.1 = {
	holder = 1023887
}

679.2.1 = {
	holder = 1024977
}

716.2.1 = {
	holder = 1025629
}

717.2.1 = {
	holder = 1026929
}

900.2.1 = {
	holder = 1035612
}

937.2.1 = {
	holder = 1035848
}

955.2.1 = {
	holder = 1037619
}

960.2.1 = {
	holder = 1038405
}

964.2.1 = {
	holder = 1036924
}

989.2.1 = {
	holder = 1037913
}

996.2.1 = {
	holder = 1040171
}

1040.2.1 = {
	holder = 1040715
}

1064.2.1 = {
	holder = 1042472
}

