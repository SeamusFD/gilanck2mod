200.2.1 = {
	holder = 1000007
	law = succ_gavelkind
	law = agnatic_succession
}

200.2.2 = {
	liege = d_temenes
}

200.2.3 = {
	liege = d_lulinik
}

200.2.4 = {
	holder = 1000052
	law = succ_primogeniture
	law = cognatic_succession
}

223.2.1 = {
	holder = 1001414
}

261.2.1 = {
	holder = 1003429
}

267.2.1 = {
	holder = 1005235
}

300.2.1 = {
	holder = 1005243
}

315.2.1 = {
	holder = 1007034
}

324.2.1 = {
	holder = 1007966
}

341.2.1 = {
	holder = 1006940
}

341.2.2 = {
	holder = 1008738
}

346.2.1 = {
	holder = 1007363
}

349.2.1 = {
	holder = 1007770
}

360.2.1 = {
	holder = 1009387
}

365.2.1 = {
	holder = 1009848
}

378.2.1 = {
	holder = 1009905
}

388.2.1 = {
	holder = 1010696
}

424.2.1 = {
	holder = 1012301
}

489.2.1 = {
	holder = 1013496
}

493.2.1 = {
	holder = 1013851
}

493.2.2 = {
	holder = 1014134
}

500.2.1 = {
	holder = 1016429
}

600.2.1 = {
	holder = 1020255
}

613.2.1 = {
	holder = 1020256
}

613.2.2 = {
	holder = 1021885
}

700.2.1 = {
	holder = 1023805
}

720.2.1 = {
	holder = 1025050
}

733.2.1 = {
	holder = 1027136
}

741.2.1 = {
	holder = 1027886
}

764.2.1 = {
	holder = 1028691
}

771.2.1 = {
	holder = 1029034
}

771.2.2 = {
	holder = 1029540
}

900.2.1 = {
	holder = 1030915
}

900.2.2 = {
	holder = 1034932
}

900.2.3 = {
	holder = 1035709
}

1000.2.1 = {
	holder = 1038015
}

1004.2.1 = {
	holder = 1039364
}

1020.2.1 = {
	holder = 1039925
}

1020.2.2 = {
	holder = 1041362
}

1028.2.1 = {
	holder = 1041540
}

1036.2.1 = {
	holder = 1039925
}

1036.2.2 = {
	holder = 1042156
}

