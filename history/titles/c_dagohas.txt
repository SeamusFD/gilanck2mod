200.2.1 = {
	holder = 1000003
	law = succ_gavelkind
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000130
	law = succ_gavelkind
	law = cognatic_succession
}

200.2.3 = {
	liege = d_egomumb
}

200.2.4 = {
	holder = 1000131
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.5 = {
	holder = 1000128
	law = succ_gavelkind
	law = cognatic_succession
}

200.2.6 = {
	holder = 1001381
}

200.2.7 = {
	holder = 1000199
}

201.2.1 = {
	holder = 1001896
}

207.2.1 = {
	holder = 1002235
}

211.2.1 = {
	holder = 1000191
}

237.2.1 = {
	holder = 1002003
}

256.2.1 = {
	holder = 1004672
}

262.2.1 = {
	holder = 1005002
}

288.2.1 = {
	holder = 1005018
}

294.2.1 = {
	holder = 1006207
}

348.2.1 = {
	holder = 1007270
}

382.2.1 = {
	holder = 1009151
}

388.2.1 = {
	holder = 1011029
}

395.2.1 = {
	holder = 1009834
}

408.2.1 = {
	holder = 1012086
}

442.2.1 = {
	holder = 1013077
}

443.2.1 = {
	holder = 1013697
}

447.2.1 = {
	holder = 1013799
}

463.2.1 = {
	holder = 1012246
}

463.2.2 = {
	holder = 1014669
}

512.2.1 = {
	holder = 1016224
}

518.2.1 = {
	holder = 1016689
}

555.2.1 = {
	holder = 1017453
}

563.2.1 = {
	holder = 1019002
}

564.2.1 = {
	holder = 1019565
}

571.2.1 = {
	holder = 1017168
}

575.2.1 = {
	holder = 1019050
}

579.2.1 = {
	holder = 1019002
}

608.2.1 = {
	holder = 1021668
}

623.2.1 = {
	holder = 1021730
}

637.2.1 = {
	holder = 1023102
}

651.2.1 = {
	holder = 1023561
}

653.2.1 = {
	holder = 1022630
}

663.2.1 = {
	holder = 1022800
}

666.2.1 = {
	holder = 1024375
}

701.2.1 = {
	holder = 1025198
}

724.2.1 = {
	holder = 1026150
}

729.2.1 = {
	holder = 1027420
}

732.2.1 = {
	holder = 1025851
}

742.2.1 = {
	holder = 1027422
}

743.2.1 = {
	holder = 1026837
}

743.2.2 = {
	holder = 1028097
}

762.2.1 = {
	holder = 1028388
}

765.2.1 = {
	holder = 1027317
}

765.2.2 = {
	holder = 1029189
}

790.2.1 = {
	holder = 1029438
}

838.2.1 = {
	holder = 1030465
}

839.2.1 = {
	holder = 1032477
}

877.2.1 = {
	holder = 1032533
}

879.2.1 = {
	holder = 1032393
}

879.2.2 = {
	holder = 1034678
}

927.2.1 = {
	holder = 1035109
}

953.2.1 = {
	holder = 1037231
}

1000.2.1 = {
	holder = 1038500
}

1003.2.1 = {
	holder = 1037387
}

1003.2.2 = {
	holder = 1040536
}

1018.2.1 = {
	holder = 1040628
}

1028.2.1 = {
	holder = 1041740
}

1051.2.1 = {
	holder = 1042661
}

1062.2.1 = {
	holder = 1043013
}

