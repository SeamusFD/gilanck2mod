200.2.1 = {
	holder = 1000017
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000018
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	holder = 1001218
	law = succ_open_elective
	law = cognatic_succession
}

200.2.4 = {
	liege = d_tasius
}

200.2.5 = {
	holder = 1001219
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.6 = {
	holder = 1001216
	law = succ_open_elective
	law = cognatic_succession
}

252.2.1 = {
	holder = 1003206
}

274.2.1 = {
	holder = 1007622
}

283.2.1 = {
	holder = 1008584
}

296.2.1 = {
	holder = 1009035
}

343.2.1 = {
	holder = 1012649
}

346.2.1 = {
	holder = 1010021
}

377.2.1 = {
	holder = 1011152
}

398.2.1 = {
	holder = 1016211
}

421.2.1 = {
	holder = 1017826
}

441.2.1 = {
	holder = 1018116
}

464.2.1 = {
	holder = 1019444
}

466.2.1 = {
	holder = 1020837
}

498.2.1 = {
	holder = 1020558
}

507.2.1 = {
	holder = 1022886
}

509.2.1 = {
	holder = 1023578
}

534.2.1 = {
	holder = 1023954
}

576.2.1 = {
	holder = 1025335
}

589.2.1 = {
	holder = 1028338
}

627.2.1 = {
	holder = 1029592
}

643.2.1 = {
	holder = 1031440
}

673.2.1 = {
	holder = 1034206
}

700.2.1 = {
	holder = 1035034
}

721.2.1 = {
	holder = 1036161
}

726.2.1 = {
	holder = 1035183
}

747.2.1 = {
	holder = 1039028
}

769.2.1 = {
	holder = 1040488
}

784.2.1 = {
	holder = 1040779
}

799.2.1 = {
	holder = 1042324
}

807.2.1 = {
	holder = 1042900
}

826.2.1 = {
	holder = 1044121
}

827.2.1 = {
	holder = 1044249
}

834.2.1 = {
	holder = 1044646
}

849.2.1 = {
	holder = 1044776
}

857.2.1 = {
	holder = 1045975
}

872.2.1 = {
	holder = 1047099
}

884.2.1 = {
	holder = 1047277
}

896.2.1 = {
	holder = 1048601
}

