200.2.1 = {
	holder = 1000006
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000590
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.3 = {
	liege = d_bavoses
}

200.2.4 = {
	holder = 1000591
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.5 = {
	holder = 1000587
	law = succ_primogeniture
	law = agnatic_succession
}

236.2.1 = {
	holder = 1003288
}

242.2.1 = {
	holder = 1004672
}

242.2.2 = {
	holder = 1005712
}

400.2.1 = {
	holder = 1015640
}

400.2.2 = {
	holder = 1016373
}

501.2.1 = {
	holder = 1022923
}

605.2.1 = {
	holder = 1027661
}

620.2.1 = {
	holder = 1027892
}

641.2.1 = {
	holder = 1030976
}

682.2.1 = {
	holder = 1033253
}

698.2.1 = {
	holder = 1034989
}

743.2.1 = {
	holder = 1037542
}

790.2.1 = {
	holder = 1038874
}

805.2.1 = {
	holder = 1042031
}

811.2.1 = {
	holder = 1039646
}

811.2.2 = {
	holder = 1043190
}

900.2.1 = {
	holder = 1043191
}

