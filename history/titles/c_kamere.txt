200.2.1 = {
	holder = 1000001
	law = succ_elective_gavelkind
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000002
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1000127
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.4 = {
	liege = d_kanava
}

200.2.5 = {
	holder = 1000123
	law = succ_primogeniture
	law = agnatic_succession
}

227.2.1 = {
	holder = 1002437
}

258.2.1 = {
	holder = 1003482
}

283.2.1 = {
	holder = 1005158
}

313.2.1 = {
	holder = 1005243
}

314.2.1 = {
	holder = 1005467
}

315.2.1 = {
	holder = 1007034
}

324.2.1 = {
	holder = 1007965
}

334.2.1 = {
	holder = 1006940
}

353.2.1 = {
	holder = 1008655
}

363.2.1 = {
	holder = 1009528
}

364.2.1 = {
	holder = 1009789
}

398.2.1 = {
	holder = 1009853
}

416.2.1 = {
	holder = 1011847
}

458.2.1 = {
	holder = 1013352
}

486.2.1 = {
	holder = 1014674
}

524.2.1 = {
	holder = 1016840
}

569.2.1 = {
	holder = 1018049
}

570.2.1 = {
	holder = 1017604
}

571.2.1 = {
	holder = 1019886
}

576.2.1 = {
	holder = 1019989
}

607.2.1 = {
	holder = 1020610
}

608.2.1 = {
	holder = 1021631
}

610.2.1 = {
	holder = 1021723
}

649.2.1 = {
	holder = 1021729
}

662.2.1 = {
	holder = 1023904
}

716.2.1 = {
	holder = 1025052
}

743.2.1 = {
	holder = 1027033
}

772.2.1 = {
	holder = 1028586
}

773.2.1 = {
	holder = 1029593
}

788.2.1 = {
	holder = 1030221
}

788.2.2 = {
	holder = 1030332
}

823.2.1 = {
	holder = 1032067
}

850.2.1 = {
	holder = 1033210
}

850.2.2 = {
	holder = 1033267
}

902.2.1 = {
	holder = 1034102
}

912.2.1 = {
	holder = 1035863
}

953.2.1 = {
	holder = 1036762
}

969.2.1 = {
	holder = 1038917
}

990.2.1 = {
	holder = 1038921
}

1005.2.1 = {
	holder = 1039873
}

1005.2.2 = {
	holder = 1037115
}

1011.2.1 = {
	holder = 1040817
}

1021.2.1 = {
	holder = 1040853
}

1021.2.2 = {
	holder = 1041448
}

1058.2.1 = {
	holder = 1042448
}

