200.2.1 = {
	holder = 1000030
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.2 = {
	holder = 1001556
	law = succ_ultimogeniture
	law = cognatic_succession
}

200.2.3 = {
	liege = d_segeorge
}

200.2.4 = {
	holder = 1001555
	law = succ_ultimogeniture
	law = cognatic_succession
}

249.2.1 = {
	holder = 1003224
}

270.2.1 = {
	holder = 1006560
}

271.2.1 = {
	holder = 1005074
}

271.2.2 = {
	holder = 1007741
}

400.2.1 = {
	holder = 1014798
}

417.2.1 = {
	holder = 1016932
}

438.2.1 = {
	holder = 1017955
}

477.2.1 = {
	holder = 1019745
}

478.2.1 = {
	holder = 1021609
}

523.2.1 = {
	holder = 1022115
}

535.2.1 = {
	holder = 1025276
}

625.2.1 = {
	holder = 1026359
}

627.2.1 = {
	holder = 1031137
}

652.2.1 = {
	holder = 1032845
}

680.2.1 = {
	holder = 1031137
}

692.2.1 = {
	holder = 1032510
}

699.2.1 = {
	holder = 1033990
}

743.2.1 = {
	holder = 1035612
}

749.2.1 = {
	holder = 1038832
}

791.2.1 = {
	holder = 1040042
}

808.2.1 = {
	holder = 1042796
}

819.2.1 = {
	holder = 1042906
}

833.2.1 = {
	holder = 1044612
}

853.2.1 = {
	holder = 1042906
}

853.2.2 = {
	holder = 1045904
}

