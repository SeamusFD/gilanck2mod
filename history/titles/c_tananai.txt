200.2.1 = {
	holder = 1000033
	law = succ_open_elective
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000034
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1001064
	law = succ_feudal_elective
	law = cognatic_succession
}

200.2.4 = {
	liege = d_rosamar
}

200.2.5 = {
	holder = 1001062
	law = succ_feudal_elective
	law = cognatic_succession
}

245.2.1 = {
	holder = 1002345
}

283.2.1 = {
	holder = 1004617
}

315.2.1 = {
	holder = 1006762
}

353.2.1 = {
	holder = 1007703
}

371.2.1 = {
	holder = 1010045
}

424.2.1 = {
	holder = 1010820
}

439.2.1 = {
	holder = 1010186
}

440.2.1 = {
	holder = 1013558
}

455.2.1 = {
	holder = 1013942
}

513.2.1 = {
	holder = 1014984
}

531.2.1 = {
	holder = 1017331
}

547.2.1 = {
	holder = 1017746
}

547.2.2 = {
	holder = 1018805
}

700.2.1 = {
	holder = 1025380
}

718.2.1 = {
	holder = 1025077
}

719.2.1 = {
	holder = 1027005
}

758.2.1 = {
	holder = 1027008
}

788.2.1 = {
	holder = 1029522
}

836.2.1 = {
	holder = 1030436
}

846.2.1 = {
	holder = 1032790
}

880.2.1 = {
	holder = 1033531
}

898.2.1 = {
	holder = 1035191
}

946.2.1 = {
	holder = 1036510
}

948.2.1 = {
	holder = 1036180
}

949.2.1 = {
	holder = 1037948
}

970.2.1 = {
	holder = 1037989
}

977.2.1 = {
	holder = 1038392
}

978.2.1 = {
	holder = 1039331
}

1066.2.1 = {
	holder = 1043289
}

