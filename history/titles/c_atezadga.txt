200.2.1 = {
	holder = 1000039
	law = succ_feudal_elective
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000040
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1001319
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.4 = {
	liege = d_qenese
}

200.2.5 = {
	holder = 1001318
	law = succ_elective_gavelkind
	law = agnatic_succession
}

226.2.1 = {
	holder = 1001979
}

257.2.1 = {
	holder = 1003337
}

270.2.1 = {
	holder = 1004880
}

322.2.1 = {
	holder = 1005933
}

326.2.1 = {
	holder = 1006888
}

333.2.1 = {
	holder = 1008428
}

370.2.1 = {
	holder = 1009829
}

403.2.1 = {
	holder = 1010784
}

425.2.1 = {
	holder = 1012440
}

440.2.1 = {
	holder = 1013479
}

450.2.1 = {
	holder = 1013171
}

450.2.2 = {
	holder = 1011299
}

500.2.1 = {
	holder = 1015307
}

528.2.1 = {
	holder = 1015554
}

532.2.1 = {
	holder = 1016170
}

532.2.2 = {
	holder = 1018039
}

566.2.1 = {
	holder = 1018154
}

575.2.1 = {
	holder = 1018041
}

575.2.2 = {
	holder = 1020133
}

586.2.1 = {
	holder = 1019188
}

602.2.1 = {
	holder = 1019971
}

626.2.1 = {
	holder = 1021575
}

634.2.1 = {
	holder = 1022970
}

641.2.1 = {
	holder = 1023061
}

651.2.1 = {
	holder = 1022735
}

652.2.1 = {
	holder = 1023747
}

652.2.2 = {
	holder = 1023749
}

672.2.1 = {
	holder = 1024586
}

693.2.1 = {
	holder = 1025394
}

724.2.1 = {
	holder = 1026817
}

747.2.1 = {
	holder = 1027780
}

792.2.1 = {
	holder = 1029484
}

802.2.1 = {
	holder = 1030973
}

842.2.1 = {
	holder = 1031020
}

864.2.1 = {
	holder = 1033834
}

865.2.1 = {
	holder = 1033926
}

865.2.2 = {
	holder = 1034086
}

879.2.1 = {
	holder = 1033795
}

879.2.2 = {
	holder = 1034714
}

935.2.1 = {
	holder = 1034886
}

939.2.1 = {
	holder = 1035812
}

942.2.1 = {
	holder = 1037073
}

943.2.1 = {
	holder = 1034926
}

1000.2.1 = {
	holder = 1037626
}

1000.2.2 = {
	holder = 1040171
}

1001.2.1 = {
	holder = 1040457
}

1048.2.1 = {
	holder = 1040468
}

1063.2.1 = {
	holder = 1043250
}

