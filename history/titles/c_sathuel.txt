200.2.1 = {
	holder = 1000003
	law = succ_tanistry
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000004
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	holder = 1000421
	law = succ_seniority
	law = cognatic_succession
}

200.2.4 = {
	liege = d_norl
}

200.2.5 = {
	holder = 1000422
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.6 = {
	liege = d_boleda
}

200.2.7 = {
	holder = 1000389
	law = succ_seniority
	law = cognatic_succession
}

212.2.1 = {
	holder = 1002930
}

253.2.1 = {
	holder = 1004489
}

284.2.1 = {
	holder = 1008454
}

303.2.1 = {
	holder = 1009849
}

347.2.1 = {
	holder = 1012281
}

389.2.1 = {
	holder = 1014647
}

440.2.1 = {
	holder = 1016052
}

457.2.1 = {
	holder = 1019831
}

472.2.1 = {
	holder = 1020916
}

490.2.1 = {
	holder = 1021991
}

507.2.1 = {
	holder = 1023276
}

561.2.1 = {
	holder = 1024408
}

578.2.1 = {
	holder = 1027380
}

624.2.1 = {
	holder = 1029723
}

642.2.1 = {
	holder = 1031659
}

667.2.1 = {
	holder = 1033037
}

667.2.2 = {
	holder = 1033763
}

693.2.1 = {
	holder = 1034691
}

733.2.1 = {
	holder = 1035786
}

743.2.1 = {
	holder = 1038748
}

779.2.1 = {
	holder = 1039094
}

791.2.1 = {
	holder = 1041204
}

816.2.1 = {
	holder = 1042574
}

868.2.1 = {
	holder = 1044064
}

872.2.1 = {
	holder = 1046941
}

876.2.1 = {
	holder = 1045989
}

877.2.1 = {
	holder = 1042145
}

900.2.1 = {
	holder = 1048554
}

