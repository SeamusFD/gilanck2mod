200.2.1 = {
	holder = 1000004
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000228
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	liege = d_nahelle
}

200.2.4 = {
	holder = 1000229
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.5 = {
	holder = 1000226
	law = succ_elective_gavelkind
	law = agnatic_succession
}

224.2.1 = {
	holder = 1000199
}

224.2.2 = {
	holder = 1003203
}

253.2.1 = {
	holder = 1003306
}

282.2.1 = {
	holder = 1004820
}

303.2.1 = {
	holder = 1006210
}

340.2.1 = {
	holder = 1007081
}

366.2.1 = {
	holder = 1009484
}

366.2.2 = {
	holder = 1009943
}

500.2.1 = {
	holder = 1014316
}

500.2.2 = {
	holder = 1016420
}

600.2.1 = {
	holder = 1020613
}

631.2.1 = {
	holder = 1021590
}

661.2.1 = {
	holder = 1023296
}

681.2.1 = {
	holder = 1022801
}

694.2.1 = {
	holder = 1024000
}

695.2.1 = {
	holder = 1025801
}

800.2.1 = {
	holder = 1027794
}

800.2.2 = {
	holder = 1028749
}

814.2.1 = {
	holder = 1031095
}

843.2.1 = {
	holder = 1032162
}

883.2.1 = {
	holder = 1033802
}

926.2.1 = {
	holder = 1036395
}

931.2.1 = {
	holder = 1035109
}

932.2.1 = {
	holder = 1037124
}

971.2.1 = {
	holder = 1037718
}

1005.2.1 = {
	holder = 1039024
}

1015.2.1 = {
	holder = 1040688
}

1021.2.1 = {
	holder = 1041416
}

1059.2.1 = {
	holder = 1043013
}

