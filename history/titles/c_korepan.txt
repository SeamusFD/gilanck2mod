200.2.1 = {
	holder = 1000016
	law = succ_tanistry
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000017
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1000551
	law = succ_tanistry
	law = cognatic_succession
}

200.2.4 = {
	liege = d_louhdaeko
}

200.2.5 = {
	holder = 1000552
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.6 = {
	holder = 1000549
	law = succ_tanistry
	law = cognatic_succession
}

204.2.1 = {
	holder = 1002109
}

235.2.1 = {
	holder = 1003066
}

275.2.1 = {
	holder = 1004156
}

301.2.1 = {
	holder = 1006010
}

314.2.1 = {
	holder = 1007429
}

322.2.1 = {
	holder = 1007869
}

327.2.1 = {
	holder = 1008121
}

371.2.1 = {
	holder = 1008144
}

396.2.1 = {
	holder = 1010269
}

417.2.1 = {
	holder = 1011525
}

431.2.1 = {
	holder = 1012641
}

433.2.1 = {
	holder = 1013125
}

444.2.1 = {
	holder = 1013103
}

444.2.2 = {
	holder = 1013774
}

451.2.1 = {
	holder = 1013974
}

475.2.1 = {
	holder = 1014682
}

506.2.1 = {
	holder = 1015628
}

536.2.1 = {
	holder = 1017363
}

547.2.1 = {
	holder = 1018798
}

557.2.1 = {
	holder = 1018821
}

565.2.1 = {
	holder = 1019351
}

565.2.2 = {
	holder = 1019623
}

606.2.1 = {
	holder = 1020056
}

638.2.1 = {
	holder = 1021844
}

680.2.1 = {
	holder = 1023350
}

704.2.1 = {
	holder = 1025121
}

707.2.1 = {
	holder = 1026316
}

714.2.1 = {
	holder = 1025594
}

714.2.2 = {
	holder = 1026771
}

717.2.1 = {
	holder = 1025594
}

717.2.2 = {
	holder = 1026935
}

740.2.1 = {
	holder = 1026951
}

773.2.1 = {
	holder = 1028046
}

774.2.1 = {
	holder = 1028266
}

794.2.1 = {
	holder = 1030620
}

819.2.1 = {
	holder = 1031932
}

835.2.1 = {
	holder = 1032489
}

872.2.1 = {
	holder = 1033865
}

883.2.1 = {
	holder = 1034688
}

927.2.1 = {
	holder = 1035661
}

939.2.1 = {
	holder = 1036994
}

959.2.1 = {
	holder = 1038384
}

959.2.2 = {
	holder = 1038435
}

995.2.1 = {
	holder = 1038807
}

1006.2.1 = {
	holder = 1040404
}

1007.2.1 = {
	holder = 1040736
}

1050.2.1 = {
	holder = 1040938
}

1055.2.1 = {
	holder = 1040598
}

1055.2.2 = {
	holder = 1043057
}

