200.2.1 = {
	holder = 1000004
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000211
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	liege = d_danrkigcata
}

200.2.4 = {
	holder = 1000208
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.5 = {
	holder = 1001381
}

200.2.6 = {
	holder = 1000199
}

247.2.1 = {
	holder = 1002071
}

259.2.1 = {
	holder = 1004707
}

261.2.1 = {
	holder = 1004672
}

265.2.1 = {
	holder = 1004856
}

283.2.1 = {
	holder = 1005937
}

355.2.1 = {
	holder = 1007071
}

363.2.1 = {
	holder = 1009484
}

367.2.1 = {
	holder = 1010005
}

379.2.1 = {
	holder = 1007772
}

380.2.1 = {
	holder = 1010643
}

431.2.1 = {
	holder = 1011104
}

442.2.1 = {
	holder = 1013181
}

443.2.1 = {
	holder = 1013077
}

468.2.1 = {
	holder = 1014882
}

491.2.1 = {
	holder = 1015414
}

526.2.1 = {
	holder = 1016422
}

553.2.1 = {
	holder = 1018171
}

581.2.1 = {
	holder = 1019748
}

587.2.1 = {
	holder = 1020679
}

616.2.1 = {
	holder = 1021879
}

643.2.1 = {
	holder = 1022697
}

682.2.1 = {
	holder = 1023597
}

703.2.1 = {
	holder = 1024547
}

704.2.1 = {
	holder = 1026263
}

721.2.1 = {
	holder = 1025935
}

767.2.1 = {
	holder = 1027187
}

773.2.1 = {
	holder = 1029189
}

784.2.1 = {
	holder = 1030135
}

793.2.1 = {
	holder = 1030142
}

799.2.1 = {
	holder = 1029438
}

838.2.1 = {
	holder = 1030465
}

839.2.1 = {
	holder = 1032477
}

839.2.2 = {
	holder = 1032821
}

871.2.1 = {
	holder = 1033271
}

913.2.1 = {
	holder = 1034616
}

942.2.1 = {
	holder = 1036349
}

963.2.1 = {
	holder = 1038219
}

976.2.1 = {
	holder = 1036672
}

982.2.1 = {
	holder = 1038424
}

988.2.1 = {
	holder = 1038022
}

988.2.2 = {
	holder = 1036438
}

991.2.1 = {
	holder = 1039825
}

1003.2.1 = {
	holder = 1039026
}

1003.2.2 = {
	holder = 1040542
}

1012.2.1 = {
	holder = 1040927
}

1021.2.1 = {
	holder = 1040628
}

1028.2.1 = {
	holder = 1041740
}

1037.2.1 = {
	holder = 1042196
}

1056.2.1 = {
	holder = 1041226
}

1057.2.1 = {
	holder = 1043125
}

