200.2.1 = {
	holder = 1000040
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1001327
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	liege = d_qayboro
}

200.2.4 = {
	liege = d_raybano
}

200.2.5 = {
	holder = 1001312
	law = succ_elective_gavelkind
	law = agnatic_succession
}

226.2.1 = {
	holder = 1002422
}

248.2.1 = {
	holder = 1003732
}

278.2.1 = {
	holder = 1004744
}

320.2.1 = {
	holder = 1006827
}

342.2.1 = {
	holder = 1006504
}

342.2.2 = {
	holder = 1008823
}

350.2.1 = {
	holder = 1008383
}

351.2.1 = {
	holder = 1009246
}

388.2.1 = {
	holder = 1009418
}

423.2.1 = {
	holder = 1011551
}

437.2.1 = {
	holder = 1012972
}

458.2.1 = {
	holder = 1014464
}

487.2.1 = {
	holder = 1014787
}

516.2.1 = {
	holder = 1015889
}

528.2.1 = {
	holder = 1017675
}

529.2.1 = {
	holder = 1015593
}

556.2.1 = {
	holder = 1019219
}

597.2.1 = {
	holder = 1020511
}

620.2.1 = {
	holder = 1021716
}

632.2.1 = {
	holder = 1021253
}

649.2.1 = {
	holder = 1021487
}

669.2.1 = {
	holder = 1023668
}

678.2.1 = {
	holder = 1024978
}

680.2.1 = {
	holder = 1025092
}

725.2.1 = {
	holder = 1026425
}

745.2.1 = {
	holder = 1028072
}

777.2.1 = {
	holder = 1028914
}

786.2.1 = {
	holder = 1030250
}

805.2.1 = {
	holder = 1031186
}

837.2.1 = {
	holder = 1032153
}

838.2.1 = {
	holder = 1029424
}

841.2.1 = {
	holder = 1031683
}

850.2.1 = {
	holder = 1033201
}

857.2.1 = {
	holder = 1031569
}

857.2.2 = {
	holder = 1033696
}

900.2.1 = {
	holder = 1033963
}

928.2.1 = {
	holder = 1036069
}

974.2.1 = {
	holder = 1038625
}

1048.2.1 = {
	holder = 1040222
}

1056.2.1 = {
	holder = 1042788
}

