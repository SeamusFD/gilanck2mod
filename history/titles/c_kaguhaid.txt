200.2.1 = {
	holder = 1000039
	law = succ_feudal_elective
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000040
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1001317
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.4 = {
	liege = d_etugano
}

200.2.5 = {
	holder = 1001315
	law = succ_elective_gavelkind
	law = agnatic_succession
}

202.2.1 = {
	holder = 1001407
}

202.2.2 = {
	holder = 1001997
}

300.2.1 = {
	holder = 1006351
}

310.2.1 = {
	holder = 1007196
}

317.2.1 = {
	holder = 1006888
}

317.2.2 = {
	holder = 1007618
}

327.2.1 = {
	holder = 1007660
}

333.2.1 = {
	holder = 1006888
}

344.2.1 = {
	holder = 1008653
}

359.2.1 = {
	holder = 1009609
}

396.2.1 = {
	holder = 1009952
}

409.2.1 = {
	holder = 1011972
}

410.2.1 = {
	holder = 1012202
}

442.2.1 = {
	holder = 1013171
}

450.2.1 = {
	holder = 1014123
}

456.2.1 = {
	holder = 1013171
}

461.2.1 = {
	holder = 1014604
}

496.2.1 = {
	holder = 1014938
}

533.2.1 = {
	holder = 1016927
}

566.2.1 = {
	holder = 1018153
}

574.2.1 = {
	holder = 1019774
}

628.2.1 = {
	holder = 1020027
}

700.2.1 = {
	holder = 1025283
}

710.2.1 = {
	holder = 1025486
}

741.2.1 = {
	holder = 1026611
}

766.2.1 = {
	holder = 1028074
}

801.2.1 = {
	holder = 1029688
}

837.2.1 = {
	holder = 1030976
}

856.2.1 = {
	holder = 1032910
}

861.2.1 = {
	holder = 1033795
}

862.2.1 = {
	holder = 1033926
}

891.2.1 = {
	holder = 1034218
}

895.2.1 = {
	holder = 1034569
}

895.2.2 = {
	holder = 1035426
}

924.2.1 = {
	holder = 1036113
}

950.2.1 = {
	holder = 1037268
}

980.2.1 = {
	holder = 1038051
}

1020.2.1 = {
	holder = 1039745
}

1030.2.1 = {
	holder = 1041735
}

1055.2.1 = {
	holder = 1042851
}

1058.2.1 = {
	holder = 1043217
}

