200.2.1 = {
	holder = 1000005
	law = succ_open_elective
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000006
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1000303
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.4 = {
	liege = d_awinudah
}

200.2.5 = {
	holder = 1000302
	law = succ_primogeniture
	law = cognatic_succession
}

229.2.1 = {
	holder = 1003059
}

242.2.1 = {
	holder = 1003807
}

245.2.1 = {
	holder = 1003097
}

245.2.2 = {
	holder = 1004185
}

293.2.1 = {
	holder = 1004355
}

294.2.1 = {
	holder = 1003651
}

294.2.2 = {
	holder = 1006469
}

323.2.1 = {
	holder = 1006570
}

341.2.1 = {
	holder = 1006569
}

342.2.1 = {
	holder = 1008790
}

500.2.1 = {
	holder = 1008918
}

500.2.2 = {
	holder = 1015214
}

506.2.1 = {
	holder = 1015147
}

506.2.2 = {
	holder = 1016774
}

515.2.1 = {
	holder = 1016793
}

559.2.1 = {
	holder = 1017761
}

582.2.1 = {
	holder = 1019408
}

606.2.1 = {
	holder = 1020724
}

619.2.1 = {
	holder = 1021594
}

625.2.1 = {
	holder = 1022348
}

632.2.1 = {
	holder = 1020906
}

632.2.2 = {
	holder = 1022850
}

800.2.1 = {
	holder = 1024196
}

802.2.1 = {
	holder = 1029694
}

802.2.2 = {
	holder = 1030986
}

900.2.1 = {
	holder = 1034622
}

901.2.1 = {
	holder = 1035646
}

904.2.1 = {
	holder = 1034761
}

905.2.1 = {
	holder = 1035907
}

915.2.1 = {
	holder = 1035955
}

933.2.1 = {
	holder = 1035529
}

934.2.1 = {
	holder = 1037237
}

1066.2.1 = {
	holder = 1041998
}

