200.2.1 = {
	holder = 1000033
	law = succ_open_elective
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000034
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1001074
	law = succ_feudal_elective
	law = cognatic_succession
}

200.2.4 = {
	liege = d_qugalt
}

200.2.5 = {
	holder = 1001073
	law = succ_feudal_elective
	law = cognatic_succession
}

244.2.1 = {
	holder = 1002939
}

276.2.1 = {
	holder = 1002845
}

277.2.1 = {
	holder = 1005661
}

285.2.1 = {
	holder = 1005719
}

294.2.1 = {
	holder = 1006488
}

362.2.1 = {
	holder = 1007489
}

363.2.1 = {
	holder = 1009764
}

500.2.1 = {
	holder = 1015434
}

533.2.1 = {
	holder = 1016205
}

546.2.1 = {
	holder = 1018298
}

547.2.1 = {
	holder = 1018775
}

558.2.1 = {
	holder = 1018780
}

597.2.1 = {
	holder = 1019927
}

622.2.1 = {
	holder = 1021999
}

623.2.1 = {
	holder = 1022376
}

640.2.1 = {
	holder = 1022472
}

664.2.1 = {
	holder = 1023318
}

680.2.1 = {
	holder = 1023048
}

681.2.1 = {
	holder = 1025130
}

800.2.1 = {
	holder = 1029020
}

807.2.1 = {
	holder = 1029421
}

822.2.1 = {
	holder = 1031340
}

831.2.1 = {
	holder = 1032459
}

870.2.1 = {
	holder = 1032791
}

900.2.1 = {
	holder = 1034508
}

916.2.1 = {
	holder = 1036327
}

927.2.1 = {
	holder = 1035502
}

928.2.1 = {
	holder = 1036958
}

928.2.2 = {
	holder = 1036957
}

934.2.1 = {
	holder = 1037149
}

940.2.1 = {
	holder = 1036180
}

957.2.1 = {
	holder = 1037305
}

989.2.1 = {
	holder = 1039848
}

1045.2.1 = {
	holder = 1041928
}

