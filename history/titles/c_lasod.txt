200.2.1 = {
	holder = 1000024
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000795
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.3 = {
	liege = d_kanica
}

200.2.4 = {
	holder = 1000796
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.5 = {
	holder = 1000793
	law = succ_primogeniture
	law = agnatic_succession
}

224.2.1 = {
	holder = 1002650
}

265.2.1 = {
	holder = 1003573
}

278.2.1 = {
	holder = 1005260
}

287.2.1 = {
	holder = 1004416
}

287.2.2 = {
	holder = 1006148
}

313.2.1 = {
	holder = 1007099
}

337.2.1 = {
	holder = 1008589
}

359.2.1 = {
	holder = 1009029
}

362.2.1 = {
	holder = 1009539
}

363.2.1 = {
	holder = 1009746
}

401.2.1 = {
	holder = 1009806
}

413.2.1 = {
	holder = 1011754
}

443.2.1 = {
	holder = 1012832
}

449.2.1 = {
	holder = 1014057
}

469.2.1 = {
	holder = 1014336
}

508.2.1 = {
	holder = 1015125
}

525.2.1 = {
	holder = 1017369
}

527.2.1 = {
	holder = 1017397
}

540.2.1 = {
	holder = 1018434
}

585.2.1 = {
	holder = 1018770
}

608.2.1 = {
	holder = 1021068
}

621.2.1 = {
	holder = 1022262
}

662.2.1 = {
	holder = 1022604
}

686.2.1 = {
	holder = 1024765
}

689.2.1 = {
	holder = 1024177
}

692.2.1 = {
	holder = 1025653
}

703.2.1 = {
	holder = 1024613
}

717.2.1 = {
	holder = 1026638
}

731.2.1 = {
	holder = 1027562
}

749.2.1 = {
	holder = 1027568
}

775.2.1 = {
	holder = 1028552
}

807.2.1 = {
	holder = 1030370
}

813.2.1 = {
	holder = 1029313
}

813.2.2 = {
	holder = 1031606
}

840.2.1 = {
	holder = 1031822
}

843.2.1 = {
	holder = 1032931
}

844.2.1 = {
	holder = 1029455
}

846.2.1 = {
	holder = 1033072
}

854.2.1 = {
	holder = 1032180
}

878.2.1 = {
	holder = 1033032
}

907.2.1 = {
	holder = 1035176
}

944.2.1 = {
	holder = 1036048
}

966.2.1 = {
	holder = 1038129
}

968.2.1 = {
	holder = 1037821
}

968.2.2 = {
	holder = 1038854
}

968.2.3 = {
	holder = 1038857
}

1011.2.1 = {
	holder = 1039884
}

1044.2.1 = {
	holder = 1041078
}

