200.2.1 = {
	holder = 1000033
	law = succ_open_elective
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000983
	law = succ_open_elective
	law = cognatic_succession
}

200.2.3 = {
	liege = d_waujri
}

200.2.4 = {
	holder = 1000984
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.5 = {
	liege = d_qunah
}

200.2.6 = {
	holder = 1000926
	law = succ_open_elective
	law = cognatic_succession
}

235.2.1 = {
	holder = 1002337
}

244.2.1 = {
	holder = 1003930
}

258.2.1 = {
	holder = 1004732
}

260.2.1 = {
	holder = 1003238
}

260.2.2 = {
	holder = 1004867
}

276.2.1 = {
	holder = 1004138
}

276.2.2 = {
	holder = 1005628
}

400.2.1 = {
	holder = 1009755
}

421.2.1 = {
	holder = 1011387
}

439.2.1 = {
	holder = 1013007
}

477.2.1 = {
	holder = 1013781
}

500.2.1 = {
	holder = 1016149
}

510.2.1 = {
	holder = 1015226
}

510.2.2 = {
	holder = 1016964
}

600.2.1 = {
	holder = 1019216
}

603.2.1 = {
	holder = 1019376
}

607.2.1 = {
	holder = 1021476
}

643.2.1 = {
	holder = 1022321
}

650.2.1 = {
	holder = 1023577
}

679.2.1 = {
	holder = 1024571
}

719.2.1 = {
	holder = 1025672
}

741.2.1 = {
	holder = 1025374
}

742.2.1 = {
	holder = 1025782
}

763.2.1 = {
	holder = 1028364
}

801.2.1 = {
	holder = 1029714
}

806.2.1 = {
	holder = 1028414
}

806.2.2 = {
	holder = 1031234
}

848.2.1 = {
	holder = 1031385
}

863.2.1 = {
	holder = 1033401
}

870.2.1 = {
	holder = 1034286
}

921.2.1 = {
	holder = 1035286
}

931.2.1 = {
	holder = 1036868
}

947.2.1 = {
	holder = 1037655
}

958.2.1 = {
	holder = 1037099
}

958.2.2 = {
	holder = 1035451
}

967.2.1 = {
	holder = 1038671
}

977.2.1 = {
	holder = 1037610
}

1003.2.1 = {
	holder = 1039040
}

1004.2.1 = {
	holder = 1040608
}

