200.2.1 = {
	holder = 1000037
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1001119
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	liege = d_tanara
}

200.2.4 = {
	holder = 1001120
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.5 = {
	liege = d_kaviif
}

200.2.6 = {
	holder = 1001106
	law = succ_primogeniture
	law = cognatic_succession
}

203.2.1 = {
	holder = 1001110
}

203.2.2 = {
	holder = 1002055
}

300.2.1 = {
	holder = 1006492
}

315.2.1 = {
	holder = 1006548
}

317.2.1 = {
	holder = 1006765
}

317.2.2 = {
	holder = 1007599
}

318.2.1 = {
	holder = 1007653
}

369.2.1 = {
	holder = 1008460
}

393.2.1 = {
	holder = 1010682
}

396.2.1 = {
	holder = 1010051
}

397.2.1 = {
	holder = 1011489
}

500.2.1 = {
	holder = 1014824
}

516.2.1 = {
	holder = 1014553
}

522.2.1 = {
	holder = 1017544
}

563.2.1 = {
	holder = 1018302
}

565.2.1 = {
	holder = 1019590
}

570.2.1 = {
	holder = 1017032
}

581.2.1 = {
	holder = 1018026
}

583.2.1 = {
	holder = 1018146
}

588.2.1 = {
	holder = 1020294
}

604.2.1 = {
	holder = 1021170
}

628.2.1 = {
	holder = 1021963
}

638.2.1 = {
	holder = 1023137
}

687.2.1 = {
	holder = 1023231
}

699.2.1 = {
	holder = 1024774
}

699.2.2 = {
	holder = 1026011
}

736.2.1 = {
	holder = 1026186
}

738.2.1 = {
	holder = 1026924
}

762.2.1 = {
	holder = 1027120
}

764.2.1 = {
	holder = 1026858
}

765.2.1 = {
	holder = 1029215
}

817.2.1 = {
	holder = 1029268
}

821.2.1 = {
	holder = 1031832
}

837.2.1 = {
	holder = 1031890
}

838.2.1 = {
	holder = 1032012
}

839.2.1 = {
	holder = 1032844
}

850.2.1 = {
	holder = 1033114
}

873.2.1 = {
	holder = 1034068
}

916.2.1 = {
	holder = 1034788
}

952.2.1 = {
	holder = 1036472
}

978.2.1 = {
	holder = 1038404
}

1009.2.1 = {
	holder = 1039598
}

1010.2.1 = {
	holder = 1037265
}

1029.2.1 = {
	holder = 1037623
}

1029.2.2 = {
	holder = 1041860
}

1042.2.1 = {
	holder = 1041884
}

