200.2.1 = {
	holder = 1000003
	law = succ_gavelkind
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000005
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1000254
	law = succ_open_elective
	law = cognatic_succession
}

200.2.4 = {
	liege = d_kagrebel
}

200.2.5 = {
	holder = 1000253
	law = succ_open_elective
	law = cognatic_succession
}

229.2.1 = {
	holder = 1001923
}

244.2.1 = {
	holder = 1003484
}

250.2.1 = {
	holder = 1004295
}

302.2.1 = {
	holder = 1005380
}

318.2.1 = {
	holder = 1006463
}

319.2.1 = {
	holder = 1007679
}

338.2.1 = {
	holder = 1006463
}

339.2.1 = {
	holder = 1008661
}

358.2.1 = {
	holder = 1009430
}

380.2.1 = {
	holder = 1010459
}

442.2.1 = {
	holder = 1011742
}

466.2.1 = {
	holder = 1014019
}

506.2.1 = {
	holder = 1016650
}

507.2.1 = {
	holder = 1015457
}

515.2.1 = {
	holder = 1016689
}

518.2.1 = {
	holder = 1017305
}

552.2.1 = {
	holder = 1017563
}

561.2.1 = {
	holder = 1017453
}

563.2.1 = {
	holder = 1019002
}

563.2.2 = {
	holder = 1016557
}

563.2.3 = {
	holder = 1019002
}

564.2.1 = {
	holder = 1019566
}

700.2.1 = {
	holder = 1023597
}

700.2.2 = {
	holder = 1023027
}

800.2.1 = {
	holder = 1029438
}

800.2.2 = {
	holder = 1030858
}

806.2.1 = {
	holder = 1031210
}

859.2.1 = {
	holder = 1032393
}

893.2.1 = {
	holder = 1034037
}

906.2.1 = {
	holder = 1035953
}

929.2.1 = {
	holder = 1036302
}

937.2.1 = {
	holder = 1037041
}

964.2.1 = {
	holder = 1038022
}

995.2.1 = {
	holder = 1039026
}

1007.2.1 = {
	holder = 1040183
}

1015.2.1 = {
	holder = 1040536
}

1018.2.1 = {
	holder = 1040628
}

1022.2.1 = {
	holder = 1041459
}

1044.2.1 = {
	holder = 1041955
}

1056.2.1 = {
	holder = 1042861
}

