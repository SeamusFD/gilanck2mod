200.2.1 = {
	holder = 1000033
	law = succ_open_elective
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000034
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1001044
	law = succ_feudal_elective
	law = cognatic_succession
}

200.2.4 = {
	liege = d_rusiog
}

200.2.5 = {
	holder = 1001045
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.6 = {
	holder = 1001042
	law = succ_feudal_elective
	law = cognatic_succession
}

206.2.1 = {
	holder = 1002054
}

213.2.1 = {
	holder = 1001008
}

235.2.1 = {
	holder = 1002052
}

266.2.1 = {
	holder = 1004171
}

298.2.1 = {
	holder = 1005600
}

303.2.1 = {
	holder = 1006486
}

322.2.1 = {
	holder = 1005919
}

328.2.1 = {
	holder = 1006695
}

336.2.1 = {
	holder = 1007896
}

337.2.1 = {
	holder = 1008590
}

353.2.1 = {
	holder = 1009344
}

412.2.1 = {
	holder = 1009463
}

421.2.1 = {
	holder = 1011138
}

437.2.1 = {
	holder = 1012730
}

446.2.1 = {
	holder = 1011907
}

446.2.2 = {
	holder = 1013884
}

468.2.1 = {
	holder = 1013991
}

475.2.1 = {
	holder = 1015132
}

534.2.1 = {
	holder = 1016459
}

541.2.1 = {
	holder = 1018298
}

552.2.1 = {
	holder = 1016822
}

561.2.1 = {
	holder = 1019426
}

615.2.1 = {
	holder = 1019673
}

630.2.1 = {
	holder = 1022242
}

634.2.1 = {
	holder = 1021999
}

643.2.1 = {
	holder = 1022713
}

644.2.1 = {
	holder = 1023402
}

655.2.1 = {
	holder = 1023407
}

660.2.1 = {
	holder = 1024133
}

691.2.1 = {
	holder = 1024868
}

699.2.1 = {
	holder = 1024077
}

701.2.1 = {
	holder = 1024818
}

703.2.1 = {
	holder = 1026228
}

716.2.1 = {
	holder = 1024249
}

724.2.1 = {
	holder = 1026346
}

739.2.1 = {
	holder = 1027930
}

771.2.1 = {
	holder = 1028179
}

804.2.1 = {
	holder = 1029619
}

810.2.1 = {
	holder = 1030061
}

825.2.1 = {
	holder = 1031062
}

859.2.1 = {
	holder = 1032672
}

908.2.1 = {
	holder = 1033829
}

912.2.1 = {
	holder = 1036222
}

920.2.1 = {
	holder = 1036614
}

921.2.1 = {
	holder = 1034869
}

922.2.1 = {
	holder = 1036695
}

922.2.2 = {
	holder = 1036222
}

946.2.1 = {
	holder = 1036973
}

949.2.1 = {
	holder = 1035929
}

950.2.1 = {
	holder = 1038012
}

958.2.1 = {
	holder = 1038194
}

1004.2.1 = {
	holder = 1039769
}

1009.2.1 = {
	holder = 1040798
}

1049.2.1 = {
	holder = 1041931
}

1061.2.1 = {
	holder = 1042883
}

