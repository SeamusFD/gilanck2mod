200.2.1 = {
	holder = 1000033
	law = succ_open_elective
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000975
	law = succ_open_elective
	law = cognatic_succession
}

200.2.3 = {
	liege = d_bogeir
}

200.2.4 = {
	holder = 1000976
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.5 = {
	holder = 1000971
	law = succ_open_elective
	law = cognatic_succession
}

244.2.1 = {
	holder = 1001648
}

253.2.1 = {
	holder = 1003538
}

254.2.1 = {
	holder = 1004611
}

265.2.1 = {
	holder = 1004371
}

266.2.1 = {
	holder = 1005174
}

295.2.1 = {
	holder = 1006180
}

296.2.1 = {
	holder = 1006588
}

305.2.1 = {
	holder = 1007007
}

329.2.1 = {
	holder = 1007145
}

330.2.1 = {
	holder = 1008262
}

356.2.1 = {
	holder = 1007900
}

356.2.2 = {
	holder = 1009461
}

386.2.1 = {
	holder = 1010924
}

428.2.1 = {
	holder = 1011818
}

440.2.1 = {
	holder = 1012730
}

441.2.1 = {
	holder = 1013618
}

450.2.1 = {
	holder = 1013667
}

461.2.1 = {
	holder = 1014588
}

501.2.1 = {
	holder = 1015692
}

519.2.1 = {
	holder = 1017092
}

530.2.1 = {
	holder = 1015922
}

531.2.1 = {
	holder = 1017961
}

541.2.1 = {
	holder = 1016761
}

569.2.1 = {
	holder = 1018938
}

619.2.1 = {
	holder = 1019923
}

629.2.1 = {
	holder = 1020262
}

630.2.1 = {
	holder = 1022712
}

669.2.1 = {
	holder = 1023094
}

679.2.1 = {
	holder = 1024282
}

684.2.1 = {
	holder = 1025030
}

692.2.1 = {
	holder = 1023777
}

693.2.1 = {
	holder = 1023781
}

699.2.1 = {
	holder = 1025877
}

701.2.1 = {
	holder = 1026127
}

714.2.1 = {
	holder = 1026501
}

770.2.1 = {
	holder = 1027574
}

772.2.1 = {
	holder = 1029020
}

772.2.2 = {
	holder = 1029572
}

804.2.1 = {
	holder = 1029898
}

804.2.2 = {
	holder = 1031121
}

825.2.1 = {
	holder = 1029974
}

826.2.1 = {
	holder = 1032228
}

833.2.1 = {
	holder = 1032556
}

848.2.1 = {
	holder = 1033249
}

863.2.1 = {
	holder = 1033306
}

874.2.1 = {
	holder = 1034159
}

920.2.1 = {
	holder = 1036465
}

943.2.1 = {
	holder = 1037305
}

989.2.1 = {
	holder = 1039848
}

1045.2.1 = {
	holder = 1041928
}

