200.2.1 = {
	holder = 1000005
	law = succ_tanistry
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000006
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	liege = d_aradelete
}

200.2.4 = {
	liege = d_sibevid
}

200.2.5 = {
	holder = 1000554
	law = succ_primogeniture
	law = agnatic_succession
}

224.2.1 = {
	holder = 1002939
}

238.2.1 = {
	holder = 1000450
}

238.2.2 = {
	holder = 1005425
}

280.2.1 = {
	holder = 1006526
}

310.2.1 = {
	holder = 1008390
}

334.2.1 = {
	holder = 1011050
}

337.2.1 = {
	holder = 1010952
}

337.2.2 = {
	holder = 1012266
}

345.2.1 = {
	holder = 1012452
}

389.2.1 = {
	holder = 1013577
}

430.2.1 = {
	holder = 1015843
}

450.2.1 = {
	holder = 1017037
}

458.2.1 = {
	holder = 1019459
}

458.2.2 = {
	holder = 1016482
}

484.2.1 = {
	holder = 1020362
}

536.2.1 = {
	holder = 1022262
}

545.2.1 = {
	holder = 1025897
}

607.2.1 = {
	holder = 1027021
}

637.2.1 = {
	holder = 1031776
}

653.2.1 = {
	holder = 1031863
}

654.2.1 = {
	holder = 1032914
}

800.2.1 = {
	holder = 1040077
}

829.2.1 = {
	holder = 1042353
}

834.2.1 = {
	holder = 1044267
}

835.2.1 = {
	holder = 1044719
}

