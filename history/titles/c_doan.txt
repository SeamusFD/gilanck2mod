200.2.1 = {
	holder = 1000033
	law = succ_open_elective
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000034
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1001041
	law = succ_feudal_elective
	law = cognatic_succession
}

200.2.4 = {
	liege = d_lyilamar
}

200.2.5 = {
	liege = d_nolilo
}

200.2.6 = {
	holder = 1001025
	law = succ_feudal_elective
	law = cognatic_succession
}

214.2.1 = {
	holder = 1001663
}

217.2.1 = {
	holder = 1002845
}

280.2.1 = {
	holder = 1003626
}

294.2.1 = {
	holder = 1006236
}

337.2.1 = {
	holder = 1007149
}

344.2.1 = {
	holder = 1008624
}

351.2.1 = {
	holder = 1007489
}

383.2.1 = {
	holder = 1009344
}

399.2.1 = {
	holder = 1011590
}

401.2.1 = {
	holder = 1009281
}

401.2.2 = {
	holder = 1011713
}

500.2.1 = {
	holder = 1015131
}

524.2.1 = {
	holder = 1016531
}

531.2.1 = {
	holder = 1015132
}

534.2.1 = {
	holder = 1016459
}

535.2.1 = {
	holder = 1018200
}

575.2.1 = {
	holder = 1018295
}

577.2.1 = {
	holder = 1017963
}

577.2.2 = {
	holder = 1020203
}

587.2.1 = {
	holder = 1020290
}

629.2.1 = {
	holder = 1021566
}

645.2.1 = {
	holder = 1022765
}

659.2.1 = {
	holder = 1023543
}

673.2.1 = {
	holder = 1024282
}

684.2.1 = {
	holder = 1025030
}

692.2.1 = {
	holder = 1023777
}

692.2.2 = {
	holder = 1025676
}

696.2.1 = {
	holder = 1025831
}

712.2.1 = {
	holder = 1025179
}

719.2.1 = {
	holder = 1026555
}

756.2.1 = {
	holder = 1027443
}

777.2.1 = {
	holder = 1029831
}

778.2.1 = {
	holder = 1028722
}

778.2.2 = {
	holder = 1028218
}

798.2.1 = {
	holder = 1030161
}

814.2.1 = {
	holder = 1031176
}

844.2.1 = {
	holder = 1032781
}

863.2.1 = {
	holder = 1033081
}

900.2.1 = {
	holder = 1033156
}

1000.2.1 = {
	holder = 1039596
}

1058.2.1 = {
	holder = 1040992
}

1061.2.1 = {
	holder = 1043341
}

