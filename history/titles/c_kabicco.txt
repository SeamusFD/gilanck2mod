200.2.1 = {
	holder = 1000038
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1001175
	law = succ_tanistry
	law = cognatic_succession
}

200.2.3 = {
	liege = d_batante
}

200.2.4 = {
	holder = 1001173
	law = succ_tanistry
	law = cognatic_succession
}

246.2.1 = {
	holder = 1002708
}

277.2.1 = {
	holder = 1004379
}

298.2.1 = {
	holder = 1005726
}

329.2.1 = {
	holder = 1007496
}

344.2.1 = {
	holder = 1008679
}

376.2.1 = {
	holder = 1009991
}

389.2.1 = {
	holder = 1010828
}

430.2.1 = {
	holder = 1011772
}

454.2.1 = {
	holder = 1013201
}

483.2.1 = {
	holder = 1014347
}

501.2.1 = {
	holder = 1015701
}

502.2.1 = {
	holder = 1014993
}

502.2.2 = {
	holder = 1016598
}

600.2.1 = {
	holder = 1019592
}

622.2.1 = {
	holder = 1020992
}

647.2.1 = {
	holder = 1022530
}

688.2.1 = {
	holder = 1025484
}

696.2.1 = {
	holder = 1023934
}

718.2.1 = {
	holder = 1024927
}

741.2.1 = {
	holder = 1025332
}

746.2.1 = {
	holder = 1027868
}

747.2.1 = {
	holder = 1028320
}

777.2.1 = {
	holder = 1029479
}

804.2.1 = {
	holder = 1030246
}

817.2.1 = {
	holder = 1031510
}

828.2.1 = {
	holder = 1031239
}

828.2.2 = {
	holder = 1032312
}

1000.2.1 = {
	holder = 1039599
}

1000.2.2 = {
	holder = 1038243
}

1001.2.1 = {
	holder = 1040459
}

