200.2.1 = {
	holder = 1000034
	law = succ_elective_gavelkind
	law = cognatic_succession
}

200.2.2 = {
	liege = d_kosuix
}

200.2.3 = {
	holder = 1001770
	law = succ_primogeniture
	law = cognatic_succession
}

240.2.1 = {
	holder = 1003125
}

269.2.1 = {
	holder = 1005765
}

271.2.1 = {
	holder = 1007748
}

276.2.1 = {
	holder = 1007914
}

277.2.1 = {
	holder = 1008175
}

310.2.1 = {
	holder = 1009500
}

311.2.1 = {
	holder = 1010479
}

349.2.1 = {
	holder = 1013043
}

367.2.1 = {
	holder = 1011092
}

368.2.1 = {
	holder = 1014282
}

378.2.1 = {
	holder = 1012366
}

389.2.1 = {
	holder = 1014309
}

389.2.2 = {
	holder = 1015633
}

391.2.1 = {
	holder = 1014281
}

406.2.1 = {
	holder = 1014409
}

430.2.1 = {
	holder = 1017125
}

458.2.1 = {
	holder = 1018935
}

461.2.1 = {
	holder = 1020507
}

475.2.1 = {
	holder = 1021419
}

493.2.1 = {
	holder = 1021534
}

532.2.1 = {
	holder = 1025053
}

555.2.1 = {
	holder = 1023874
}

571.2.1 = {
	holder = 1026844
}

576.2.1 = {
	holder = 1026401
}

577.2.1 = {
	holder = 1027925
}

612.2.1 = {
	holder = 1028261
}

643.2.1 = {
	holder = 1030284
}

656.2.1 = {
	holder = 1033092
}

662.2.1 = {
	holder = 1033352
}

680.2.1 = {
	holder = 1034367
}

698.2.1 = {
	holder = 1032380
}

699.2.1 = {
	holder = 1035888
}

731.2.1 = {
	holder = 1035620
}

732.2.1 = {
	holder = 1033351
}

743.2.1 = {
	holder = 1036491
}

743.2.2 = {
	holder = 1038776
}

786.2.1 = {
	holder = 1040606
}

787.2.1 = {
	holder = 1041579
}

792.2.1 = {
	holder = 1039073
}

807.2.1 = {
	holder = 1040606
}

808.2.1 = {
	holder = 1042958
}

848.2.1 = {
	holder = 1043834
}

879.2.1 = {
	holder = 1045707
}

