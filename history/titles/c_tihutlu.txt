200.2.1 = {
	holder = 1000003
	law = succ_tanistry
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000004
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	holder = 1000387
	law = succ_seniority
	law = cognatic_succession
}

200.2.4 = {
	liege = d_levav
}

200.2.5 = {
	holder = 1000388
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.6 = {
	liege = d_tunila
}

200.2.7 = {
	holder = 1000399
	law = succ_seniority
	law = cognatic_succession
}

242.2.1 = {
	holder = 1002931
}

246.2.1 = {
	holder = 1002630
}

259.2.1 = {
	holder = 1006007
}

271.2.1 = {
	holder = 1007299
}

296.2.1 = {
	holder = 1009386
}

313.2.1 = {
	holder = 1009773
}

314.2.1 = {
	holder = 1010442
}

354.2.1 = {
	holder = 1010835
}

362.2.1 = {
	holder = 1013061
}

362.2.2 = {
	holder = 1011196
}

377.2.1 = {
	holder = 1014829
}

395.2.1 = {
	holder = 1015976
}

397.2.1 = {
	holder = 1013921
}

398.2.1 = {
	holder = 1016169
}

415.2.1 = {
	holder = 1016904
}

423.2.1 = {
	holder = 1015783
}

424.2.1 = {
	holder = 1017988
}

447.2.1 = {
	holder = 1016973
}

451.2.1 = {
	holder = 1019817
}

456.2.1 = {
	holder = 1020092
}

463.2.1 = {
	holder = 1019453
}

509.2.1 = {
	holder = 1020915
}

513.2.1 = {
	holder = 1021554
}

534.2.1 = {
	holder = 1024544
}

549.2.1 = {
	holder = 1025891
}

599.2.1 = {
	holder = 1027310
}

600.2.1 = {
	holder = 1029340
}

611.2.1 = {
	holder = 1030115
}

621.2.1 = {
	holder = 1030185
}

653.2.1 = {
	holder = 1031335
}

666.2.1 = {
	holder = 1033037
}

701.2.1 = {
	holder = 1034469
}

744.2.1 = {
	holder = 1036523
}

761.2.1 = {
	holder = 1039472
}

778.2.1 = {
	holder = 1040681
}

779.2.1 = {
	holder = 1040434
}

780.2.1 = {
	holder = 1041081
}

819.2.1 = {
	holder = 1041265
}

833.2.1 = {
	holder = 1044134
}

887.2.1 = {
	holder = 1045806
}

888.2.1 = {
	holder = 1045989
}

889.2.1 = {
	holder = 1048151
}

