200.2.1 = {
	holder = 1000005
	law = succ_open_elective
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000006
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1000294
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.4 = {
	liege = d_nionm
}

200.2.5 = {
	holder = 1000292
	law = succ_primogeniture
	law = cognatic_succession
}

209.2.1 = {
	holder = 1002007
}

211.2.1 = {
	holder = 1000306
}

231.2.1 = {
	holder = 1002600
}

263.2.1 = {
	holder = 1003651
}

296.2.1 = {
	holder = 1005381
}

324.2.1 = {
	holder = 1006620
}

325.2.1 = {
	holder = 1007987
}

500.2.1 = {
	holder = 1014305
}

510.2.1 = {
	holder = 1015111
}

518.2.1 = {
	holder = 1017340
}

700.2.1 = {
	holder = 1024501
}

710.2.1 = {
	holder = 1026152
}

721.2.1 = {
	holder = 1025549
}

721.2.2 = {
	holder = 1027087
}

763.2.1 = {
	holder = 1028039
}

795.2.1 = {
	holder = 1029304
}

847.2.1 = {
	holder = 1030693
}

853.2.1 = {
	holder = 1033335
}

855.2.1 = {
	holder = 1031861
}

859.2.1 = {
	holder = 1033746
}

863.2.1 = {
	holder = 1031423
}

867.2.1 = {
	holder = 1033470
}

873.2.1 = {
	holder = 1034399
}

917.2.1 = {
	holder = 1035392
}

944.2.1 = {
	holder = 1036678
}

956.2.1 = {
	holder = 1037974
}

1008.2.1 = {
	holder = 1039028
}

1023.2.1 = {
	holder = 1040871
}

1029.2.1 = {
	holder = 1039925
}

1029.2.2 = {
	holder = 1041818
}

