200.2.1 = {
	holder = 1000034
	law = succ_elective_gavelkind
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000035
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	holder = 1001763
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.4 = {
	liege = d_nulika
}

200.2.5 = {
	holder = 1001764
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.6 = {
	holder = 1001760
	law = succ_primogeniture
	law = cognatic_succession
}

239.2.1 = {
	holder = 1004156
}

240.2.1 = {
	holder = 1003516
}

244.2.1 = {
	holder = 1003125
}

259.2.1 = {
	holder = 1006900
}

270.2.1 = {
	holder = 1006786
}

271.2.1 = {
	holder = 1007746
}

273.2.1 = {
	holder = 1007914
}

307.2.1 = {
	holder = 1009500
}

329.2.1 = {
	holder = 1011092
}

378.2.1 = {
	holder = 1012366
}

389.2.1 = {
	holder = 1014309
}

390.2.1 = {
	holder = 1015693
}

408.2.1 = {
	holder = 1016021
}

428.2.1 = {
	holder = 1018026
}

429.2.1 = {
	holder = 1017153
}

461.2.1 = {
	holder = 1018674
}

474.2.1 = {
	holder = 1020507
}

485.2.1 = {
	holder = 1017501
}

490.2.1 = {
	holder = 1020573
}

500.2.1 = {
	holder = 1018098
}

600.2.1 = {
	holder = 1024251
}

700.2.1 = {
	holder = 1032618
}

701.2.1 = {
	holder = 1035620
}

743.2.1 = {
	holder = 1036491
}

782.2.1 = {
	holder = 1039073
}

792.2.1 = {
	holder = 1039309
}

811.2.1 = {
	holder = 1042122
}

834.2.1 = {
	holder = 1043394
}

869.2.1 = {
	holder = 1044760
}

889.2.1 = {
	holder = 1047267
}

893.2.1 = {
	holder = 1048393
}

