200.2.1 = {
	holder = 1000005
	law = succ_tanistry
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000019
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	holder = 1001253
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.4 = {
	liege = d_elilla
}

200.2.5 = {
	holder = 1001249
	law = succ_primogeniture
	law = agnatic_succession
}

225.2.1 = {
	holder = 1001246
}

225.2.2 = {
	holder = 1004602
}

262.2.1 = {
	holder = 1004772
}

287.2.1 = {
	holder = 1007533
}

324.2.1 = {
	holder = 1009413
}

351.2.1 = {
	holder = 1011933
}

386.2.1 = {
	holder = 1013296
}

405.2.1 = {
	holder = 1015562
}

434.2.1 = {
	holder = 1016858
}

443.2.1 = {
	holder = 1019220
}

471.2.1 = {
	holder = 1021063
}

490.2.1 = {
	holder = 1022082
}

500.2.1 = {
	holder = 1019936
}

500.2.2 = {
	holder = 1019997
}

515.2.1 = {
	holder = 1023235
}

530.2.1 = {
	holder = 1024234
}

569.2.1 = {
	holder = 1026024
}

602.2.1 = {
	holder = 1029551
}

648.2.1 = {
	holder = 1032539
}

702.2.1 = {
	holder = 1035957
}

723.2.1 = {
	holder = 1037252
}

778.2.1 = {
	holder = 1040252
}

799.2.1 = {
	holder = 1041523
}

799.2.2 = {
	holder = 1042320
}

834.2.1 = {
	holder = 1042664
}

872.2.1 = {
	holder = 1046152
}

873.2.1 = {
	holder = 1046222
}

873.2.2 = {
	holder = 1047134
}

