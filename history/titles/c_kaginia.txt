200.2.1 = {
	holder = 1000003
	law = succ_gavelkind
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000004
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1000201
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.4 = {
	liege = d_telle
}

200.2.5 = {
	holder = 1000199
	law = succ_elective_gavelkind
	law = agnatic_succession
}

201.2.1 = {
	holder = 1001897
}

300.2.1 = {
	holder = 1005518
}

335.2.1 = {
	holder = 1006562
}

362.2.1 = {
	holder = 1009484
}

364.2.1 = {
	holder = 1009835
}

425.2.1 = {
	holder = 1010119
}

428.2.1 = {
	holder = 1012935
}

481.2.1 = {
	holder = 1014128
}

500.2.1 = {
	holder = 1016178
}

539.2.1 = {
	holder = 1017117
}

559.2.1 = {
	holder = 1018655
}

571.2.1 = {
	holder = 1019894
}

609.2.1 = {
	holder = 1021103
}

621.2.1 = {
	holder = 1022036
}

628.2.1 = {
	holder = 1022629
}

657.2.1 = {
	holder = 1022801
}

694.2.1 = {
	holder = 1024000
}

712.2.1 = {
	holder = 1025803
}

714.2.1 = {
	holder = 1025851
}

715.2.1 = {
	holder = 1026783
}

800.2.1 = {
	holder = 1029237
}

813.2.1 = {
	holder = 1030259
}

848.2.1 = {
	holder = 1031984
}

864.2.1 = {
	holder = 1032393
}

864.2.2 = {
	holder = 1033982
}

1000.2.1 = {
	holder = 1039560
}

1007.2.1 = {
	holder = 1040334
}

1012.2.1 = {
	holder = 1038546
}

1012.2.2 = {
	holder = 1040972
}

