200.2.1 = {
	holder = 1000006
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000579
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.3 = {
	liege = d_nelete
}

200.2.4 = {
	holder = 1000580
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.5 = {
	holder = 1000577
	law = succ_primogeniture
	law = agnatic_succession
}

225.2.1 = {
	holder = 1003465
}

261.2.1 = {
	holder = 1004759
}

263.2.1 = {
	holder = 1002796
}

263.2.2 = {
	holder = 1007192
}

313.2.1 = {
	holder = 1010316
}

340.2.1 = {
	holder = 1008603
}

340.2.2 = {
	holder = 1008711
}

400.2.1 = {
	holder = 1012434
}

506.2.1 = {
	holder = 1021602
}

506.2.2 = {
	holder = 1023368
}

600.2.1 = {
	holder = 1028595
}

624.2.1 = {
	holder = 1028982
}

641.2.1 = {
	holder = 1031597
}

678.2.1 = {
	holder = 1034223
}

689.2.1 = {
	holder = 1032889
}

689.2.2 = {
	holder = 1035207
}

716.2.1 = {
	holder = 1035790
}

717.2.1 = {
	holder = 1037127
}

750.2.1 = {
	holder = 1039156
}

755.2.1 = {
	holder = 1037998
}

755.2.2 = {
	holder = 1039513
}

781.2.1 = {
	holder = 1039532
}

817.2.1 = {
	holder = 1041030
}

817.2.2 = {
	holder = 1043600
}

