200.2.1 = {
	holder = 1000005
	law = succ_tanistry
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000507
	law = succ_tanistry
	law = cognatic_succession
}

200.2.3 = {
	liege = d_bomade
}

200.2.4 = {
	holder = 1000508
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.5 = {
	liege = d_sibevid
}

200.2.6 = {
	holder = 1000554
	law = succ_primogeniture
	law = agnatic_succession
}

224.2.1 = {
	holder = 1002939
}

238.2.1 = {
	holder = 1000450
}

238.2.2 = {
	holder = 1005425
}

280.2.1 = {
	holder = 1006526
}

310.2.1 = {
	holder = 1008390
}

334.2.1 = {
	holder = 1011050
}

337.2.1 = {
	holder = 1010952
}

337.2.2 = {
	holder = 1012266
}

345.2.1 = {
	holder = 1012452
}

389.2.1 = {
	holder = 1013577
}

430.2.1 = {
	holder = 1015843
}

450.2.1 = {
	holder = 1017037
}

458.2.1 = {
	holder = 1019459
}

458.2.2 = {
	holder = 1016482
}

484.2.1 = {
	holder = 1020362
}

536.2.1 = {
	holder = 1022262
}

545.2.1 = {
	holder = 1025897
}

607.2.1 = {
	holder = 1027021
}

637.2.1 = {
	holder = 1031776
}

638.2.1 = {
	holder = 1031862
}

653.2.1 = {
	holder = 1031863
}

673.2.1 = {
	holder = 1033708
}

691.2.1 = {
	holder = 1034934
}

707.2.1 = {
	holder = 1036046
}

712.2.1 = {
	holder = 1033452
}

712.2.2 = {
	holder = 1036812
}

758.2.1 = {
	holder = 1036997
}

761.2.1 = {
	holder = 1038339
}

761.2.2 = {
	holder = 1039912
}

789.2.1 = {
	holder = 1040077
}

829.2.1 = {
	holder = 1042353
}

834.2.1 = {
	holder = 1044267
}

835.2.1 = {
	holder = 1044718
}

845.2.1 = {
	holder = 1044796
}

846.2.1 = {
	holder = 1045398
}

860.2.1 = {
	holder = 1046135
}

861.2.1 = {
	holder = 1046394
}

883.2.1 = {
	holder = 1044589
}

884.2.1 = {
	holder = 1047805
}

