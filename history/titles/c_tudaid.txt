200.2.1 = {
	holder = 1000039
	law = succ_feudal_elective
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000040
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1001337
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.4 = {
	liege = d_waroro
}

200.2.5 = {
	holder = 1001336
	law = succ_elective_gavelkind
	law = agnatic_succession
}

228.2.1 = {
	holder = 1002424
}

267.2.1 = {
	holder = 1003468
}

291.2.1 = {
	holder = 1005282
}

293.2.1 = {
	holder = 1006407
}

320.2.1 = {
	holder = 1006967
}

356.2.1 = {
	holder = 1009041
}

385.2.1 = {
	holder = 1009771
}

404.2.1 = {
	holder = 1011089
}

431.2.1 = {
	holder = 1013146
}

449.2.1 = {
	holder = 1013377
}

455.2.1 = {
	holder = 1014348
}

464.2.1 = {
	holder = 1014252
}

465.2.1 = {
	holder = 1014739
}

469.2.1 = {
	holder = 1014788
}

491.2.1 = {
	holder = 1012707
}

492.2.1 = {
	holder = 1016073
}

600.2.1 = {
	holder = 1019680
}

624.2.1 = {
	holder = 1021034
}

625.2.1 = {
	holder = 1020171
}

626.2.1 = {
	holder = 1022539
}

800.2.1 = {
	holder = 1029128
}

808.2.1 = {
	holder = 1029139
}

812.2.1 = {
	holder = 1030326
}

813.2.1 = {
	holder = 1031629
}

838.2.1 = {
	holder = 1031894
}

882.2.1 = {
	holder = 1033008
}

908.2.1 = {
	holder = 1035337
}

965.2.1 = {
	holder = 1036114
}

966.2.1 = {
	holder = 1036704
}

967.2.1 = {
	holder = 1038824
}

1066.2.1 = {
	holder = 1042520
}

