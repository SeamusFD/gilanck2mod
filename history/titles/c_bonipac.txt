200.2.1 = {
	holder = 1000000
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	liege = d_lulinik
}

200.2.3 = {
	liege = d_egutar
}

200.2.4 = {
	holder = 1000337
	law = succ_gavelkind
	law = agnatic_succession
}

239.2.1 = {
	holder = 1002009
}

244.2.1 = {
	holder = 1003966
}

257.2.1 = {
	holder = 1001377
}

257.2.2 = {
	holder = 1004749
}

322.2.1 = {
	holder = 1006002
}

344.2.1 = {
	holder = 1007928
}

356.2.1 = {
	holder = 1009064
}

357.2.1 = {
	holder = 1009257
}

358.2.1 = {
	holder = 1009529
}

500.2.1 = {
	holder = 1015411
}

510.2.1 = {
	holder = 1014305
}

510.2.2 = {
	holder = 1016983
}

600.2.1 = {
	holder = 1019993
}

612.2.1 = {
	holder = 1021009
}

636.2.1 = {
	holder = 1021884
}

666.2.1 = {
	holder = 1023854
}

711.2.1 = {
	holder = 1024948
}

741.2.1 = {
	holder = 1027243
}

759.2.1 = {
	holder = 1028152
}

782.2.1 = {
	holder = 1028700
}

800.2.1 = {
	holder = 1030869
}

900.2.1 = {
	holder = 1033135
}

901.2.1 = {
	holder = 1034104
}

902.2.1 = {
	holder = 1035769
}

947.2.1 = {
	holder = 1035774
}

963.2.1 = {
	holder = 1037930
}

972.2.1 = {
	holder = 1038881
}

983.2.1 = {
	holder = 1038165
}

995.2.1 = {
	holder = 1039828
}

996.2.1 = {
	holder = 1039787
}

1032.2.1 = {
	holder = 1040136
}

1034.2.1 = {
	holder = 1041282
}

1037.2.1 = {
	holder = 1040727
}

