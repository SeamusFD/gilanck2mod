200.2.1 = {
	holder = 1000001
	law = succ_elective_gavelkind
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000002
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1000117
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.4 = {
	liege = d_eviralay
}

200.2.5 = {
	holder = 1000118
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.6 = {
	liege = d_wemire
}

200.2.7 = {
	holder = 1000095
	law = succ_primogeniture
	law = agnatic_succession
}

244.2.1 = {
	holder = 1001916
}

270.2.1 = {
	holder = 1004394
}

281.2.1 = {
	holder = 1005375
}

333.2.1 = {
	holder = 1006124
}

343.2.1 = {
	holder = 1008473
}

355.2.1 = {
	holder = 1009257
}

364.2.1 = {
	holder = 1009387
}

368.2.1 = {
	holder = 1009849
}

368.2.2 = {
	holder = 1010014
}

374.2.1 = {
	holder = 1010297
}

392.2.1 = {
	holder = 1010401
}

420.2.1 = {
	holder = 1011271
}

460.2.1 = {
	holder = 1012988
}

490.2.1 = {
	holder = 1014569
}

492.2.1 = {
	holder = 1016002
}

506.2.1 = {
	holder = 1014674
}

506.2.2 = {
	holder = 1016741
}

600.2.1 = {
	holder = 1017643
}

700.2.1 = {
	holder = 1024048
}

709.2.1 = {
	holder = 1024942
}

729.2.1 = {
	holder = 1026776
}

741.2.1 = {
	holder = 1027229
}

742.2.1 = {
	holder = 1028029
}

768.2.1 = {
	holder = 1026979
}

768.2.2 = {
	holder = 1029345
}

774.2.1 = {
	holder = 1029391
}

793.2.1 = {
	holder = 1030227
}

807.2.1 = {
	holder = 1031262
}

840.2.1 = {
	holder = 1031413
}

863.2.1 = {
	holder = 1030301
}

864.2.1 = {
	holder = 1031735
}

876.2.1 = {
	holder = 1034565
}

910.2.1 = {
	holder = 1034710
}

932.2.1 = {
	holder = 1036523
}

951.2.1 = {
	holder = 1036671
}

963.2.1 = {
	holder = 1038627
}

976.2.1 = {
	holder = 1038633
}

1013.2.1 = {
	holder = 1039781
}

1014.2.1 = {
	holder = 1041059
}

