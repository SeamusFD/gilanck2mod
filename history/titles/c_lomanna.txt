200.2.1 = {
	holder = 1000004
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000013
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1000382
	law = succ_gavelkind
	law = cognatic_succession
}

200.2.4 = {
	liege = d_nelophie
}

200.2.5 = {
	holder = 1000380
	law = succ_gavelkind
	law = cognatic_succession
}

200.2.6 = {
	holder = 1001384
}

249.2.1 = {
	holder = 1002072
}

255.2.1 = {
	holder = 1004533
}

260.2.1 = {
	holder = 1004883
}

263.2.1 = {
	holder = 1005060
}

280.2.1 = {
	holder = 1005237
}

299.2.1 = {
	holder = 1006037
}

314.2.1 = {
	holder = 1007456
}

326.2.1 = {
	holder = 1008041
}

382.2.1 = {
	holder = 1008251
}

388.2.1 = {
	holder = 1010645
}

388.2.2 = {
	holder = 1011030
}

414.2.1 = {
	holder = 1010645
}

425.2.1 = {
	holder = 1011215
}

430.2.1 = {
	holder = 1013101
}

475.2.1 = {
	holder = 1013302
}

493.2.1 = {
	holder = 1015448
}

530.2.1 = {
	holder = 1016311
}

571.2.1 = {
	holder = 1019938
}

588.2.1 = {
	holder = 1017891
}

596.2.1 = {
	holder = 1020926
}

601.2.1 = {
	holder = 1020757
}

601.2.2 = {
	holder = 1021373
}

700.2.1 = {
	holder = 1024690
}

711.2.1 = {
	holder = 1025888
}

714.2.1 = {
	holder = 1026766
}

731.2.1 = {
	holder = 1026824
}

753.2.1 = {
	holder = 1028233
}

802.2.1 = {
	holder = 1031031
}

836.2.1 = {
	holder = 1031251
}

874.2.1 = {
	holder = 1033168
}

885.2.1 = {
	holder = 1034567
}

891.2.1 = {
	holder = 1035252
}

931.2.1 = {
	holder = 1035470
}

951.2.1 = {
	holder = 1037434
}

962.2.1 = {
	holder = 1037392
}

973.2.1 = {
	holder = 1038429
}

988.2.1 = {
	holder = 1039553
}

994.2.1 = {
	holder = 1039431
}

999.2.1 = {
	holder = 1040229
}

1000.2.1 = {
	holder = 1040401
}

1066.2.1 = {
	holder = 1041823
}

