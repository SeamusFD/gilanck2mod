200.2.1 = {
	holder = 1000006
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000007
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	holder = 1000625
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.4 = {
	liege = d_dohyu
}

200.2.5 = {
	holder = 1000624
	law = succ_primogeniture
	law = agnatic_succession
}

230.2.1 = {
	holder = 1003817
}

263.2.1 = {
	holder = 1004185
}

264.2.1 = {
	holder = 1007220
}

312.2.1 = {
	holder = 1008137
}

329.2.1 = {
	holder = 1010848
}

341.2.1 = {
	holder = 1008595
}

342.2.1 = {
	holder = 1012514
}

500.2.1 = {
	holder = 1020133
}

500.2.2 = {
	holder = 1022991
}

600.2.1 = {
	holder = 1026316
}

605.2.1 = {
	holder = 1027886
}

627.2.1 = {
	holder = 1029861
}

658.2.1 = {
	holder = 1031447
}

668.2.1 = {
	holder = 1033645
}

687.2.1 = {
	holder = 1033809
}

687.2.2 = {
	holder = 1032050
}

706.2.1 = {
	holder = 1035115
}

749.2.1 = {
	holder = 1037965
}

752.2.1 = {
	holder = 1037067
}

753.2.1 = {
	holder = 1039347
}

900.2.1 = {
	holder = 1045393
}

