200.2.1 = {
	holder = 1000032
	law = succ_primogeniture
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000033
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1001001
	law = succ_open_elective
	law = cognatic_succession
}

200.2.4 = {
	liege = d_estudis
}

200.2.5 = {
	holder = 1000999
	law = succ_open_elective
	law = cognatic_succession
}

206.2.1 = {
	holder = 1000894
}

229.2.1 = {
	holder = 1003427
}

231.2.1 = {
	holder = 1000894
}

246.2.1 = {
	holder = 1002194
}

258.2.1 = {
	holder = 1004756
}

288.2.1 = {
	holder = 1005626
}

289.2.1 = {
	holder = 1005919
}

328.2.1 = {
	holder = 1006695
}

336.2.1 = {
	holder = 1007896
}

348.2.1 = {
	holder = 1008949
}

350.2.1 = {
	holder = 1008624
}

352.2.1 = {
	holder = 1008590
}

362.2.1 = {
	holder = 1009727
}

371.2.1 = {
	holder = 1010185
}

390.2.1 = {
	holder = 1011145
}

403.2.1 = {
	holder = 1010183
}

416.2.1 = {
	holder = 1012472
}

420.2.1 = {
	holder = 1010186
}

420.2.2 = {
	holder = 1012650
}

433.2.1 = {
	holder = 1013090
}

493.2.1 = {
	holder = 1013938
}

507.2.1 = {
	holder = 1016817
}

522.2.1 = {
	holder = 1015922
}

523.2.1 = {
	holder = 1017590
}

561.2.1 = {
	holder = 1016822
}

573.2.1 = {
	holder = 1017963
}

589.2.1 = {
	holder = 1020592
}

616.2.1 = {
	holder = 1021999
}

626.2.1 = {
	holder = 1022522
}

673.2.1 = {
	holder = 1023660
}

697.2.1 = {
	holder = 1024917
}

737.2.1 = {
	holder = 1026002
}

760.2.1 = {
	holder = 1028174
}

764.2.1 = {
	holder = 1028217
}

772.2.1 = {
	holder = 1029262
}

775.2.1 = {
	holder = 1029020
}

775.2.2 = {
	holder = 1029718
}

778.2.1 = {
	holder = 1028722
}

778.2.2 = {
	holder = 1029899
}

807.2.1 = {
	holder = 1029935
}

837.2.1 = {
	holder = 1032046
}

838.2.1 = {
	holder = 1032783
}

853.2.1 = {
	holder = 1032839
}

887.2.1 = {
	holder = 1034507
}

891.2.1 = {
	holder = 1034159
}

920.2.1 = {
	holder = 1036465
}

943.2.1 = {
	holder = 1037305
}

957.2.1 = {
	holder = 1038338
}

983.2.1 = {
	holder = 1038616
}

1025.2.1 = {
	holder = 1039894
}

1046.2.1 = {
	holder = 1041781
}

