200.2.1 = {
	holder = 1000037
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000038
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	liege = d_raaldis
}

200.2.4 = {
	liege = d_rohiaco
}

200.2.5 = {
	holder = 1001190
	law = succ_tanistry
	law = cognatic_succession
}

203.2.1 = {
	holder = 1001704
}

258.2.1 = {
	holder = 1004128
}

261.2.1 = {
	holder = 1003282
}

261.2.2 = {
	holder = 1004942
}

279.2.1 = {
	holder = 1004021
}

280.2.1 = {
	holder = 1005815
}

282.2.1 = {
	holder = 1005532
}

283.2.1 = {
	holder = 1005974
}

315.2.1 = {
	holder = 1007494
}

322.2.1 = {
	holder = 1005318
}

323.2.1 = {
	holder = 1007901
}

332.2.1 = {
	holder = 1008019
}

365.2.1 = {
	holder = 1009821
}

394.2.1 = {
	holder = 1009828
}

402.2.1 = {
	holder = 1011450
}

404.2.1 = {
	holder = 1011875
}

450.2.1 = {
	holder = 1012535
}

468.2.1 = {
	holder = 1014350
}

469.2.1 = {
	holder = 1014426
}

470.2.1 = {
	holder = 1014992
}

519.2.1 = {
	holder = 1015550
}

537.2.1 = {
	holder = 1018307
}

563.2.1 = {
	holder = 1018593
}

588.2.1 = {
	holder = 1020067
}

594.2.1 = {
	holder = 1020948
}

599.2.1 = {
	holder = 1020656
}

614.2.1 = {
	holder = 1021971
}

628.2.1 = {
	holder = 1022546
}

651.2.1 = {
	holder = 1023708
}

662.2.1 = {
	holder = 1024241
}

719.2.1 = {
	holder = 1025626
}

753.2.1 = {
	holder = 1027171
}

776.2.1 = {
	holder = 1028679
}

779.2.1 = {
	holder = 1028008
}

780.2.1 = {
	holder = 1026241
}

780.2.2 = {
	holder = 1028282
}

781.2.1 = {
	holder = 1030022
}

829.2.1 = {
	holder = 1031675
}

849.2.1 = {
	holder = 1033043
}

903.2.1 = {
	holder = 1034252
}

904.2.1 = {
	holder = 1034073
}

905.2.1 = {
	holder = 1035934
}

929.2.1 = {
	holder = 1034215
}

930.2.1 = {
	holder = 1037066
}

934.2.1 = {
	holder = 1036517
}

934.2.2 = {
	holder = 1034079
}

935.2.1 = {
	holder = 1037267
}

983.2.1 = {
	holder = 1038097
}

996.2.1 = {
	holder = 1040167
}

1031.2.1 = {
	holder = 1040268
}

1057.2.1 = {
	holder = 1042129
}

