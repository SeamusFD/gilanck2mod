200.2.1 = {
	holder = 1000003
	law = succ_gavelkind
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000004
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	holder = 1000193
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.4 = {
	liege = d_silier
}

200.2.5 = {
	holder = 1000194
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.6 = {
	holder = 1000191
	law = succ_elective_gavelkind
	law = agnatic_succession
}

237.2.1 = {
	holder = 1002003
}

260.2.1 = {
	holder = 1004104
}

297.2.1 = {
	holder = 1005517
}

310.2.1 = {
	holder = 1006653
}

366.2.1 = {
	holder = 1007524
}

383.2.1 = {
	holder = 1010261
}

447.2.1 = {
	holder = 1011514
}

462.2.1 = {
	holder = 1014067
}

505.2.1 = {
	holder = 1015457
}

515.2.1 = {
	holder = 1016689
}

515.2.2 = {
	holder = 1015262
}

534.2.1 = {
	holder = 1017303
}

551.2.1 = {
	holder = 1016422
}

551.2.2 = {
	holder = 1018960
}

700.2.1 = {
	holder = 1025851
}

742.2.1 = {
	holder = 1027422
}

743.2.1 = {
	holder = 1026837
}

743.2.2 = {
	holder = 1028096
}

758.2.1 = {
	holder = 1028251
}

799.2.1 = {
	holder = 1029751
}

827.2.1 = {
	holder = 1031938
}

829.2.1 = {
	holder = 1031805
}

830.2.1 = {
	holder = 1032388
}

882.2.1 = {
	holder = 1033603
}

898.2.1 = {
	holder = 1034845
}

948.2.1 = {
	holder = 1035722
}

975.2.1 = {
	holder = 1038019
}

988.2.1 = {
	holder = 1038022
}

988.2.2 = {
	holder = 1039785
}

