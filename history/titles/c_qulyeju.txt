200.2.1 = {
	holder = 1000035
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1000036
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	holder = 1001790
	law = succ_feudal_elective
	law = cognatic_succession
}

200.2.4 = {
	liege = d_lifeem
}

200.2.5 = {
	holder = 1001789
	law = succ_feudal_elective
	law = cognatic_succession
}

233.2.1 = {
	holder = 1003783
}

273.2.1 = {
	holder = 1005209
}

288.2.1 = {
	holder = 1008889
}

312.2.1 = {
	holder = 1009089
}

365.2.1 = {
	holder = 1011024
}

377.2.1 = {
	holder = 1014486
}

437.2.1 = {
	holder = 1016255
}

442.2.1 = {
	holder = 1019305
}

458.2.1 = {
	holder = 1019567
}

462.2.1 = {
	holder = 1018326
}

469.2.1 = {
	holder = 1018368
}

478.2.1 = {
	holder = 1021099
}

534.2.1 = {
	holder = 1022181
}

541.2.1 = {
	holder = 1025532
}

572.2.1 = {
	holder = 1025037
}

582.2.1 = {
	holder = 1026451
}

612.2.1 = {
	holder = 1027795
}

613.2.1 = {
	holder = 1030285
}

648.2.1 = {
	holder = 1030416
}

672.2.1 = {
	holder = 1032681
}

689.2.1 = {
	holder = 1034371
}

724.2.1 = {
	holder = 1035497
}

757.2.1 = {
	holder = 1039250
}

758.2.1 = {
	holder = 1039705
}

759.2.1 = {
	holder = 1039772
}

769.2.1 = {
	holder = 1037330
}

770.2.1 = {
	holder = 1040539
}

819.2.1 = {
	holder = 1043023
}

824.2.1 = {
	holder = 1042874
}

837.2.1 = {
	holder = 1044896
}

862.2.1 = {
	holder = 1045638
}

893.2.1 = {
	holder = 1046746
}

