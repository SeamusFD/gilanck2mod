200.2.1 = {
	holder = 1000041
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.2 = {
	holder = 1001805
	law = succ_primogeniture
	law = cognatic_succession
}

200.2.3 = {
	liege = d_qamemos
}

200.2.4 = {
	holder = 1001806
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.5 = {
	holder = 1001803
	law = succ_primogeniture
	law = cognatic_succession
}

239.2.1 = {
	holder = 1002479
}

248.2.1 = {
	holder = 1006134
}

249.2.1 = {
	holder = 1006201
}

251.2.1 = {
	holder = 1006354
}

266.2.1 = {
	holder = 1006640
}

291.2.1 = {
	holder = 1008047
}

310.2.1 = {
	holder = 1010043
}

320.2.1 = {
	holder = 1011165
}

323.2.1 = {
	holder = 1011239
}

326.2.1 = {
	holder = 1011552
}

339.2.1 = {
	holder = 1012367
}

375.2.1 = {
	holder = 1014742
}

414.2.1 = {
	holder = 1015699
}

449.2.1 = {
	holder = 1017419
}

470.2.1 = {
	holder = 1019880
}

536.2.1 = {
	holder = 1021408
}

539.2.1 = {
	holder = 1022028
}

546.2.1 = {
	holder = 1025584
}

570.2.1 = {
	holder = 1026578
}

579.2.1 = {
	holder = 1028070
}

618.2.1 = {
	holder = 1029752
}

654.2.1 = {
	holder = 1031823
}

657.2.1 = {
	holder = 1033139
}

693.2.1 = {
	holder = 1034437
}

731.2.1 = {
	holder = 1036557
}

774.2.1 = {
	holder = 1039378
}

806.2.1 = {
	holder = 1040833
}

832.2.1 = {
	holder = 1042964
}

845.2.1 = {
	holder = 1044690
}

847.2.1 = {
	holder = 1045497
}

855.2.1 = {
	holder = 1045566
}

868.2.1 = {
	holder = 1046747
}

900.2.1 = {
	holder = 1048251
}

