200.2.1 = {
	holder = 1000022
	law = succ_tanistry
	law = agnatic_succession
}

200.2.2 = {
	holder = 1000043
	law = succ_elective_gavelkind
	law = agnatic_succession
}

200.2.3 = {
	holder = 1001854
	law = succ_ultimogeniture
	law = agnatic_succession
}

200.2.4 = {
	liege = d_sabiltet
}

200.2.5 = {
	holder = 1001853
	law = succ_ultimogeniture
	law = agnatic_succession
}

200.2.6 = {
	holder = 1002027
}

245.2.1 = {
	holder = 1003530
}

268.2.1 = {
	holder = 1006797
}

272.2.1 = {
	holder = 1005896
}

273.2.1 = {
	holder = 1007899
}

400.2.1 = {
	holder = 1007900
}

400.2.2 = {
	holder = 1011226
}

400.2.3 = {
	holder = 1012060
}

500.2.1 = {
	holder = 1022304
}

538.2.1 = {
	holder = 1022467
}

543.2.1 = {
	holder = 1022168
}

552.2.1 = {
	holder = 1024080
}

570.2.1 = {
	holder = 1026790
}

572.2.1 = {
	holder = 1025793
}

573.2.1 = {
	holder = 1027670
}

615.2.1 = {
	holder = 1028807
}

616.2.1 = {
	holder = 1030488
}

645.2.1 = {
	holder = 1030684
}

673.2.1 = {
	holder = 1033863
}

679.2.1 = {
	holder = 1034262
}

679.2.2 = {
	holder = 1034604
}

684.2.1 = {
	holder = 1031040
}

693.2.1 = {
	holder = 1034658
}

700.2.1 = {
	holder = 1032429
}

800.2.1 = {
	holder = 1040400
}

815.2.1 = {
	holder = 1042127
}

857.2.1 = {
	holder = 1043732
}

878.2.1 = {
	holder = 1046240
}

